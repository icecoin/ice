/* global __dirname, require, module*/

const webpack = require('webpack');
const path = require('path');
const env = require('yargs').argv.env; // use --env with webpack 2
const pkg = require('./package.json');
var ExtractTextPlugin = require("extract-text-webpack-plugin")


let libraryName = pkg.name;

let outputFile, mode;

if (env === 'build') {
  mode = 'production';
  outputFile = libraryName + '.min.js';
} else {
  mode = 'development';
  outputFile = libraryName + '.js';
}

var react_loader = {loader: "babel-loader",options: {presets: ['es2015', 'stage-0', 'react']}}

const config = {
  // mode: mode,
  // entry: __dirname + '/src/index.js',
  entry: __dirname + '/src/ice.js',
  devtool: 'inline-source-map',
  output: {
    path: __dirname + '/lib',
    filename: outputFile,
    library: libraryName,
    libraryTarget: 'umd',
    umdNamedDefine: true,
    publicPath: './lib/',
    // libraryTarget: 'var',
    // library: 'AppEntryPoint'
    // globalObject: "typeof self !== 'undefined' ? self : this"
  },
  module: {
    rules: [
      {
        test: /(\.jsx|\.js)$/,
        use: react_loader,
        exclude: /(node_modules|bower_components)/
      },
      {
        test: /\.(svg|png|jpg|jpeg|gif)$/,
        use: {loader: "file-loader", options: {name: 'imgs/[name].[ext]'}}
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: "css-loader" }
        ]
      },
      {
        test: /\.(eot|ttf|woff|svg|svgz|otf).*$/,
        use: {loader: "file-loader", options: {name: 'fonts/[name].[ext]' }}
      },
      // {
      //   test: /(\.jsx|\.js)$/,
      //   loader: 'eslint-loader',
      //   exclude: /node_modules/
      // }
    ]
  },
  resolve: {
    modules: [path.resolve('./node_modules'), path.resolve('./src')],
    extensions: ['.json', '.js']
  },
  externals: {
    "d3": 'd3',
    react: {
        root: 'React',
        commonjs2: 'react',
        commonjs: 'react',
        amd: 'react'
    },
    'react-dom': {
        root: 'ReactDOM',
        commonjs2: 'react-dom',
        commonjs: 'react-dom',
        amd: 'react-dom'
    }
  }
};

module.exports = config;

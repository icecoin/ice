const gen_range = (len) => {
  var l = [];
  for(var i = 0; i < len; i++) {
    l.push(i)
  }
  return l;
}

const group_by = (xs, mapper) => {
  return xs.reduce((acc, x) => {
    const k = mapper(x);
    if (acc[k]) {
      acc[k].push(x)
    } else {
      acc[k] = [x];
    }
    return acc;
  }, {});
}


// 1 : put everything in state
// 2 : loop through each for initial positioning
// 3 : use square after that



const reducers = {
  init_position: (game, e) => {
    // const state = {name: "choose_action", params: {player: game.players[0].id} };
    const {players, positions} = game;

    const auto_event = {e: "position_initial_next_roll"};

    const state = {
      name: "position_initial_rolls",
      params: {players: players.map(p => { return {id: p.id, done: false, split_done: false} })}
    }
    return {...game, state, auto_event};
  },
  position_initial_next_roll: (game, e) => {
    const { players } = game.state.params;

    if (players.length == 0) {
      const state = {name: "position_loop_dice_roll"}
      const auto_event = {e: "position_ask_next"};
      return {...game, state, auto_event};
    } else {
      const state = {
        name: "position_initial_dice_roll",
        params: {players, player: players[0].id},
        accept_events: ["position_initial_dice_roll_done"]
      }
      return {...game, state};
    }
  },
  position_initial_dice_roll_done: (game, e) => {
    const {dice_roll} = e.params;
    const {positions, state: {params: {players: [player, ...rest_players]}}} = game;

    // add player to rank square
    const new_positions = positions.map(pos => {
      return pos.rank == dice_roll ? {...pos, players: pos.players.concat([player])} : pos
    })

    const auto_event = {e: "position_initial_next_roll"};
    const state = {name: "position_initial_rolls", params: {players: rest_players}};

    return {...game, state, auto_event, positions: new_positions};
  },

  position_ask_next: (game, e) => {
    const {positions} = game;

    // const all_done = positions.every(p => !p.players.length || p.players.every(p => p.done))
    const players_group = positions.map(p => p.players).filter(ps => ps.length != 0)
    const players = [].concat.apply([], players_group);


    // dont ask for very last player if he is done or alone on the last square
    // missing: if 30
    const last_player = players[players.length - 1];
    const last_player_done = last_player.done || players_group[players_group.length - 1].length == 1;

    const all_done = last_player_done && players.slice(0, players.length-1).every(p => p.done)


    // everyone is happy trying, starts splitting results if necessary
    if (all_done) {
      const auto_event = {e: "position_init_split_next"};
      const state = {name: "position_init_split", params: {players_group, players: []}};
      var g = {...game, state, auto_event};
      delete g.positions;

      return g
    }


    const {id: player} = players.find(p => !p.done);

    const state = {name: "position_ask", params: {player}}
    return {...game, state};
  },
  position_ask_answer: (game, e) => {
    const {continue_roll} = e.params
    let {positions, state: {params: {player}}} = game;

    if (continue_roll) {
      const state = {name: "position_dice_roll", params: {player}, accept_events: ["position_dice_roll_done"]}
      return {...game, state};
    } else {
      // done for this player
      positions = positions.map(pos => {
        if (pos.players.find(p => p.id == player)) {
          const players = pos.players.map(p => p.id == player ? {...p, done: true} : p)
          return {...pos, players};
        } else {
          return pos;
        }
      });
      return {...game, positions, state: {}, auto_event: {e: "position_ask_next"}};
    }
  },
  position_dice_roll_done: (game, e) => {
    const {dice_roll} = e.params
    let {positions, state: {params: {player}}} = game;

    const prev_pos = positions.find(pos => pos.players.find(p => p.id == player));
    const done = dice_roll < prev_pos.rank;

    const prev_player = prev_pos.players.find(p => p.id == player);
    const new_player = {...prev_player, done};

    console.log(`Player ${player} : new rank ${dice_roll} (was ${prev_pos.rank})`);

    positions = positions.map(pos => {
      if (pos.rank == dice_roll) {
        return {...pos, players: [new_player].concat(pos.players)}
      } else if (pos.rank == prev_pos.rank) {
        return {...pos, players: pos.players.filter(p => p.id != player)}
      } else {
        return pos;
      }
    })

    return {...game, positions, state: {}, auto_event: {e: "position_ask_next"}};
  },

  position_init_split_next: (game, e) => {
    // players_group: group to split ; players: already ranked so far
    const { players_group, players } = game.state.params;

    var idx = players_group.findIndex(pg => pg.length > 1);
    idx = idx === -1 ? players_group.length + 1 : idx;

    const done = players_group.slice(0, idx).map(([p]) => p.id);
    const pending_pgroups = players_group.slice(idx);

    const done_players = players.concat(done)
    const [pgroup, ...rest_pgroups] = pending_pgroups;

    if (! pgroup) {
      const state = {name: "position_all_done", params: {players: done_players}};
      return {...game, state, auto_event: {e: "position_all_done"}};
    } else {
      const state = {
        name: "state_position_split_next",
        params: {players: done_players, players_group: rest_pgroups, pgroup}
      };
      return {...game, state, auto_event: {e: "position_split_next"}};
    }
  },

  // resolve group
  position_split_next: (game, e) => {
    const { pgroup, players, players_group } = game.state.params;
    const p = pgroup.find(p => ! p.split_dice_roll);

    // every player in group has roll, we can sort now
    if (!p) {
      const p_by_dice = group_by(pgroup, x => x.split_dice_roll);
      const dice_sort_asc = Object.keys(p_by_dice).map(x => parseInt(x)).sort((a, b) => a - b);

      const pgroups = dice_sort_asc.map(dice_key => {
        // set dice_roll to null
        return p_by_dice[dice_key].map(x => { return {...x, split_dice_roll: null}; });
      });

      const state = {
        name: "state_position_init_split_next",
        params: { players, players_group: pgroups.concat(players_group) }
      };

      return {...game, state, auto_event: {e: "position_init_split_next"}}
    } else {
      const state = {
        name: "position_split_dice_roll",
        params: {player: p.id, pgroup, players, players_group},
        accept_events: ["position_split_dice_roll_done"]
      };
      return {...game, state}
    }
  },

  position_split_dice_roll_done: (game, e) => {
    const {dice_roll} = e.params
    const {player, pgroup, players, players_group} = game.state.params

    // add dice_roll to player in group
    const new_pgroup = pgroup.map(p => p.id == player ? {...p, split_dice_roll: dice_roll} : p)

    const state = {name: "state_position_split_next", params: {pgroup: new_pgroup, players, players_group}};

    return {...game, state, auto_event: {e: "position_split_next"}};
  },


  position_all_done: (game, e) => {
    const { players } = game.state.params;


    // players is sorted from the last to the first: reverse, so the first is first
    const auto_event = {e: "init", params: {players: players.slice().reverse()}};
    var g = {...game, auto_event};

    // clean the mess
    delete g.positions;
    delete g.state;

    return g;
  }

}


const getReducers = () => { return {...reducers}; }

export { getReducers }


// mind

// gen_range of length len
const gen_range = (len) => {
  var l = [];
  for(var i = 0; i < len; i++) {
    l.push(i)
  }
  return l;
}


const defaultCtrl = () => {
  const cb = () => false
  return {
    player: { onClick: cb },
    square: { onClick: cb, onMouseOver: cb, onMouseOut: cb },
  };
}

var state_handlers = [];

const position_dice_roll = {
  name: "position_dice_roll",
  handle: ({name}) => ["position_initial_dice_roll", "position_dice_roll", "position_split_dice_roll"].indexOf(name) !== -1,
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const {state: {player, name, accept_events: [e]}, opts} = getGame();

    const nb_dice = 1;
    const nb = 30;

    // auto roll dice
    const rollDice = () => (Math.floor(Math.random() * 100) % nb) + 1;
    const random_dice_rolls = gen_range(nb_dice).map(rollDice)

    const goNext = (dice_rolls = random_dice_rolls) => {
      sendEvent({
        e, params: { dice_roll: dice_rolls[0] }
      })
    }

    const action = () => {
      return <div>
      <div className="state-title">Dé {nb}</div>
        { opts.dice_roll_manual && <div className="choice-list three-col">
          { gen_range(nb).map(i => {
              return <div className="choice-item" key={i} onClick={() => goNext([i + 1])}>{ i + 1 }</div>
            })
          }
        </div> }
      </div>
    }

    return {...ctrl, goNext, action};
  },
  exec: (ctrl, game) => {
    if (!game.opts.dice_roll_manual) {
      setTimeout(() => { ctrl.goNext() }, 2000)
    }
  }
}


const position_ask = {
  name: "position_ask",
  handle: ({name}) => { return name == "position_ask"; },
  create: (getGame, {render, sendEvent}) => {
    const {player} = getGame().state.params
    const ctrl = defaultCtrl();

    const goNext = (continue_roll) => {
      sendEvent({
        e: "position_ask_answer",
        params: { continue_roll }
      })
    }

    const action = () => {
      return <div>
      <div className="state-title">Phase d'attaque du joueur {player}</div>
        <div className="state-info"></div>
        <div className="choice-list">
          <div className="choice-item" onClick={() => goNext(true)}>Continuer</div>
          <div className="choice-item" onClick={() => goNext(false)}>S'arrêter</div>
        </div>
      </div>
    }

    return {...ctrl, action}
  },
}


const handlers = [
  position_dice_roll,
  position_ask,
]

const getHandlers = () => handlers.slice()

export { getHandlers }


import React from 'react';
import * as d3 from "d3";


import img_surprise from "../static/images/surprise.png";
import img_trampo from "../static/images/swirl_trampoline.png";
import img_hole from "../static/images/hole.png";
import img_ko from "../static/images/dizzy4.png";


const Position = React.createClass({
  render() {
    const {game, position} = this.props;
    const {players} = game;
    const {id, players: pos_players, x, y, height, width, center, rank} = position;

    var s = {height, width, fill: "white", stroke: "black", strokeWidth: 2}


    return <svg x={x} y={y}>
      <rect style={s} />
      <text x={width/2} y={height/2} style={{dominantBaseline:"middle", textAnchor: "middle"}}>{rank}</text>

      <g>
        { pos_players.map(({id, done}, i) => {
            const p_center = {x: width/2, y: height/2 + (height/3*i)};
            return <Player key={i} game={game} id={id} center={p_center} />
          })
        }
      </g>
    </svg>

  }
})


// force_style is for circle
const MiniPlayer = ({ player, wrap_svg, force_style, extra_style, is_phacoch }) => {
  const {color, url} = player;

  const {r, x, y} = is_phacoch ? {r: 50, x: -10, y: -10} : {r: 35, x: 0, y: 0};

  const side = r * 2;
  const circle_s = force_style || {cx: r, cy: r, r, fill: color}


  // { url ?  <image xlinkHref={img_surprise} width={width - 10 + "px"} height={height - 10 + "px"} x={x + 5} y={y + 5} />
  const inner = url ? <image xlinkHref={url} width={side + "px"} height={side + "px"} x={x} y={y} style={extra_style || {}} /> : <circle style={circle_s} />;

  if (wrap_svg) {
   return <svg className="turn-player-fig" style={{width: `${side}px`, height: `${side}px`}}>
    { inner }
   </svg>
  } else {
    return inner;
  }
}

const Square = React.createClass({
  getInitialState() {
    return {overed: false, destination_interval: null, destination_opacity: 1, destination_vec: 0};
  },
  onClick() {
    this.props.game.ctrl.square.onClick(this.props.square.id)
  },
  onMouseOver() {
    if (this.props.square.destination) {
      const destination_interval = setInterval(() => {
        var {destination_opacity, destination_vec} = this.state;

        if (destination_opacity >= 1) {
          destination_vec = -0.1
        } else if (destination_opacity <= 0.3) {
          destination_vec = 0.1
        }

        destination_opacity += destination_vec;

        this.setState({ destination_opacity, destination_vec })
      }, 100)

      this.setState({destination_interval, destination_opacity: 1})
    }
    this.setState({overed: true})

    this.props.game.ctrl.square.onMouseOver(this.props.square.id);
  },
  onMouseOut() {
    this.setState({overed: false})
    if (this.state.destination_interval) {
      clearInterval(this.state.destination_interval)
      this.setState({destination_interval: null, destination_opacity: 1})
    }

    this.props.game.ctrl.square.onMouseOut(this.props.square.id);
  },
  colorFromType(type) {
    return {
      flat: "white",
      increase: "darkturquoise",
      // "cornerflowerblue",
      // deepskyblue
      // royalblue navy skyblue
      decrease: "dodgerblue",
      trampoline: "lightgreen",
    }[type]
  },
  render() {
    const {overed, destination_opacity} = this.state;
    const {game, square} = this.props;
    const {squares_show, squares_show_special, squares_show_effect} = game.opts;
    const {players} = game;
    const {id, type, x, y, height, width, center, selected, no_fill,
           destination, player, inflight, phacoch, in_destination, in_move_path, special, effect} = square;

    const selected_style = {"stroke": "aqua", "strokeWidth": 2};
    const overed_style = {"stroke": "lime", "strokeWidth": 4};
    const overed_selected_style = {"stroke": "crimson", "strokeWidth": 2};

    var s = {height, width, x: 0, y: 0,
             fill: no_fill ? "rgba(0,0,0,0)" : this.colorFromType(type), stroke: "#555", strokeWidth: 0}

    var className = "";

    if (selected && overed) {
      s = {...s, ...overed_selected_style}
    } else if(selected) {
      s = {...s, ...selected_style}
    } else if(overed) {
      s = {...s, ...overed_style}

      if (in_destination) {
        if (destination) {
          className += "cursor-move";
        } else {
          className += "cursor-not";
        }
      }
    }

    const destination_style = destination ? {...s, fill: "black", opacity: 0.3} : {};

    const show_destination = destination && overed;

    const squares_show_display = {
      id: id,
      // rank: `W: ${square.djk.weight} ${square.djk.weight_sub ||''} ; R: ${square.djk.rank}`
      rank: `rank: ${square.rank}`
    };

    var special_style = {};
    const sside = 20;

    if (special == "start") {
      special_style = {fill: "green", width: sside, height: sside, x: width-sside, y: height-sside}
    } else if (special == "end") {
      special_style = {fill: "red", width: sside, height: sside, x: width-sside, y: height-sside}
    } else if (special == "sprint") {
      special_style = {fill: "orange", width: sside, height: sside, x: width-sside, y: height-sside}
    }

    var effect_style = {};
    if (effect == "hole") {
      effect_style = {fill: "black", width: sside, height: sside, x: width-sside, y: 0}
    } else if (effect == "hurdle") {
      effect_style = {fill: "yellow", width: sside, height: sside, x: width-sside, y: 0}
    } else if (effect == "surprise") {
      effect_style = {fill: "darkviolet", width: sside, height: sside, x: width-sside, y: 0}
    }

    const is_trampo = type == "trampoline";
    const is_surprise = effect == "surprise";
    const is_hole = effect == "hole";

    return <g className={className} onClick={this.onClick} onMouseEnter={this.onMouseOver} onMouseLeave={this.onMouseOut} transform={`translate(${x},${y})`} >
      { destination && <rect style={s} /> }
      <rect style={destination ? destination_style : s} />

      { squares_show_special && special && <rect style={special_style} /> }
      { squares_show_effect && effect && <rect style={effect_style} /> }

      { is_surprise && <image xlinkHref={img_surprise} width={width - 10 + "px"} height={height - 10 + "px"} x={5} y={5} /> }
      { is_trampo && <image xlinkHref={img_trampo} width={width + 20 + "px"} height={height + 20 + "px"} x={- 10} y={- 10} /> }
      { is_hole && <image xlinkHref={img_hole} width={width - 10 + "px"} height={height - 10 + "px"} x={5} y={5} /> }

      <g style={{opacity: show_destination ? destination_opacity : 1}} >
        { (player || show_destination) &&
          <Player game={game} id={player || destination} is_destination={show_destination} center={center} /> }
      </g>

      { in_move_path && <g style={{opacity: 0.3}}>
        <Player game={game} id={in_move_path} center={center} /> }
      </g> }

      { inflight && <Player game={game} id={inflight} center={center} /> }

      { phacoch && <Phacoch game={game} center={center} /> }

      { squares_show && <text x={0} y={height/2} style={{}}>
          { squares_show_display.id  + ": " + squares_show_display.rank }
        </text> }
    </g>

  }
})


const Player = React.createClass({
  getInitialState() {
    return {overed: false};
  },
  onClick() {
    this.props.game.ctrl.player.onClick(this.props.id)
  },
  onMouseOver() {
    this.setState({overed: true})
  },
  onMouseOut() {
    this.setState({overed: false})
  },
  render() {
    const {overed} = this.state;
    const {game, id, center, is_destination} = this.props;
    const {x, y} = center;
    const player = game.players.find(p => p.id == id)
    const {status, color, r, selected} = player;

    const selected_style = {"stroke": "chartreuse", "strokeWidth": 2};
    const overed_style = {"stroke": "aqua", "strokeWidth": 2};
    const overed_selected_style = {"stroke": "crimson", "strokeWidth": 2};

    var s = {cx: x, cy: y, r, fill: color, fillOpacity: "0.4"}

    var className = "";

    if (!is_destination) {
      if (selected && overed) {
        s = {...s, ...overed_selected_style}
      } else if(selected) {
        s = {...s, ...selected_style}
      } else if(overed) {
        s = {...s, ...overed_style}
      }
    } else {
      s = {...s, stroke: "crimson", strokeWidth: 2}
    }

    const fixme = -20;
    const text_style = {x: s.cx + fixme, y: s.cy + s.r + 15, textAlign: "center"}
    const side = 35*2; // fixme

    const is_ko = status == "ko";
    const extra_style = is_ko ? {opacity: 0.7} : {};

    // <text style={text_style} x={text_style.x} y={text_style.y} >COUCOU</text>
    return <g onClick={is_destination ? null : this.onClick} onMouseOver={this.onMouseOver} onMouseOut={this.onMouseOut}>
      <MiniPlayer player={player} force_style={s} extra_style={extra_style} />
      { is_ko && <image xlinkHref={img_ko} width={side + "px"} height={side + "px"} x={0} y={0} /> }
    </g>
  }
})


const Phacoch = React.createClass({
  render() {
    const {game, center} = this.props;
    const {x, y} = center;
    const {status, color, r, selected} = game.phacoch
    const is_ko = status == "ko";

    const s = {cx: x, cy: y, r, fill: color}
    const side = 50*2; // fixme
    const extra_style = is_ko ? {opacity: 0.7} : {};

    return <g>
      <MiniPlayer player={game.phacoch} force_style={s} is_phacoch={true} extra_style={extra_style} />
      { is_ko && <image xlinkHref={img_ko} width={side + "px"} height={side + "px"} x={0} y={0} /> }
    </g>
  }
})


const SquareLines = React.createClass({

  render() {
    const {squares} = this.props;

    const centers = squares.map(({id, next, center}) => {
      return next.map((s2_id) => {
        return [center, squares.find(({id}) => id == s2_id).center]
      })
    })

    const flat_centers = [].concat.apply([], centers);

    const style = { strokeWidth: "6px", stroke: "pink" };

    return <g> {
      flat_centers.map(([c1, c2], i) => {
          /*
          var diff_x = c2.x > c1.x ? 1 : -1;
          var diff_y = c2.y > c1.y ? 1 : -1;
          var d = 10;
          */
          var diff_x = 1, diff_y = 1, d = 0;
          return <line key={i} x1={c1.x - d*diff_x} y1={c1.y - d*diff_y} x2={c2.x + d*diff_x} y2={c2.y + d*diff_y} style={style} markerEnd={"url(#head)"} />
      })
    } </g>
  }

})


const TurnOverview = React.createClass({
  render() {
    const {players, turn} = this.props.game;

    // ko, race_turn
    const TurnPlayer = ({ p, turn }) => {
      const r = 35, side = r*2;

      const is_ko = p.status == "ko";

      return <div className="turn-player">
        <div style={{textAlign: "center"}}>
          <div style={{display: "flex"}}>

            <div style={{position: "relative"}}>
              <MiniPlayer player={p} wrap_svg={true} />
              { is_ko && <div className="turn-player-ko"
                  style={{width: side + "px", height: side +"px"}}>
                  <div className="turn-player-ko-turn" style={{backgroundSize: side +"px"}}/>
                  <span style={{lineHeight: side + "px"}}>{ p.rise_on_turn - turn.i + 1 }</span>
              </div> }
            </div>

            <div className="player-side-info">
              <div className="player-race-turn">{ p.race_turn }</div>
              <div className="player-skull">{ p.skulls }</div>
            </div>

          </div>

          <div className="player-name">{ p.id } </div>
        </div>
      </div>
    }

    return <div className="turn-overview">

      <div className="turn-overview-header">
        Tour <span className="game-turn-count">#{turn.i}</span>
      </div>

      { turn.players.length > 0 && <div className="turn-ingame">
        { turn.players.map((tp, i) => {
          const p = players.find(({id}) => id == tp.player)

          const classes = `turn-player-wrapper ${tp.current ? "turn-player-wrapper-current" : ""}`

          return <div key={i} className={classes}>
            <TurnPlayer p={p} current={tp.current} turn={turn} />
          </div>
        }) }
      </div> }

      <div className="turn-overview-footer" />
    </div>
  }
})


const EliminatedOverview = React.createClass({
  getInitialState() {
    return {hide: true};
  },
  toggle() {
    this.setState({hide: !this.state.hide})
  },
  render() {
    const {hide} = this.state
    const elim_players = this.props.players;
    const {players, turn} = this.props.game;
    const count = elim_players.length;

    return <div className={"eliminated-overview" + (hide?" closed":"")}>
      <div className="eliminated-overview-header" onClick={this.toggle}>
        <div className="position-head eliminated-count">
          <span>{ count }</span>
        </div>
        <div className="position-body title">Élimination</div>
      </div>

      <div className="eliminated-overview-player-list">

      { elim_players.map((p, i) => {
        return <div key={i} className="eliminated-player-wrapper">
          <div className="position-head"></div>
          <div className="position-body eliminated-player">
            <MiniPlayer player={p} wrap_svg={true} />
            <div className="player-name">{ p.id } </div>
          </div>
        </div>
      }) }

      </div>

      <div className="eliminated-overview-footer" />
    </div>
  }
})


const FinishOverview = React.createClass({
  getInitialState() {
    return {hide: true};
  },
  toggle() {
    this.setState({hide: !this.state.hide})
  },
  render() {
    const {hide} = this.state
    const finish_players = this.props.players;
    const {players, turn} = this.props.game;
    const count = finish_players.length;

    return <div className={"finish-overview" + (hide?" closed":"")}>
      <div className="finish-overview-header" onClick={this.toggle}>
        <div className="position-head finish-count">
          <span>{ count }</span>
        </div>
        <div className="position-body title">Arrivée</div>
      </div>

      <div className="finish-overview-player-list">

      { finish_players.map((p, i) => {
        return <div key={i} className="finish-player-wrapper">
          <div className={`position-head finish-rank finish-rank-${p.finish_nth}`}>
            <span>{ p.finish_nth + 1 }</span>
          </div>
          <div className="position-body finish-player">
            <MiniPlayer player={p} wrap_svg={true} />
            <div className="player-name">{ p.id } </div>
          </div>
        </div>
      }) }

      </div>

      <div className="finish-overview-footer" />
    </div>
  }
})


const GameOverview = React.createClass({
  render() {
    const {players, turn} = this.props.game;

    const finished = players.filter(p => p.status == "finish");
    const elim = players.filter(p => p.status == "eliminated");

    return <div className="game-overview">

      { turn && turn.players && turn.players.length > 0 && <TurnOverview game={this.props.game} /> }

      <div className="outgame-overview">
        { <FinishOverview game={this.props.game} players={finished} /> }
        { <EliminatedOverview game={this.props.game} players={elim} /> }
      </div>
    </div>
  }
})


const GameCtrl = React.createClass({
  getInitialState() {
    const maps = [
      "Anneau", "Aspirator",
      "Carioca", "Circulator", "Cirque Ulator", "Classic", "Colonel",
      "Espace", "Espana", "FightClub", "LaBasic", "London",
      "Magnifico", "MarcAngel", "Marshmallow", "Mexico",
      "MicroMania", "Milan", "Monaco", "Mortel",
      "Paris", "PerfectSize", "PerfectSize2", "Premiere",
      "TestMini", "TestQuick"
    ];
    return { maps, closed: true };
  },
  chooseMap(map) {
    this.props.ice_ctrl.changeMap(map)
  },
  toggle() {
    this.setOpenClose(!this.state.closed);
  },
  setOpenClose(closed) {
    this.setState({ closed })
  },
  render() {
    const {maps, closed} = this.state;

    return <div className="game-ctrl-bar">
      <div className="map-choose-wrapper">
        <div className={"map-choose" + (closed?" map-closed":"")} onMouseLeave={() => this.setOpenClose(true)}>
          <div className="map-button" onClick={this.toggle} />
          <ul className="map-list">
            { maps.map((x, i) => <li key={i} onClick={() => this.chooseMap(x)}>{ x }</li>) }
          </ul>
        </div>
      </div>
    </div>
  }
})


const GameStreamInfo = React.createClass({
  getInitialState() {
    const info = [];
    return {info, last: null};
  },
  componentWillReceiveProps(nextProps) {
    if (nextProps.info === this.state.last) {
      return
    }

    const new_info = this.addData(nextProps.info).map(this.trans).filter(x => x);
    const info = this.state.info.concat(new_info).slice(-15);

    this.setState({ info, last: nextProps.info });
  },
  transState(value) {
    const with_ppfx = ({ params }, rest) => <span><b>{`[${ params.player }] `}</b>{ rest }</span>

    const d = {
      choose_action: (v) => with_ppfx(v, "Choix d'une action"),
      breathe_action: (v) => with_ppfx(v, "Choix d'une case de respiration"),
      // "state_loop_turn"

    }

    return (d[value.name] || (() => false))(value)
  },
  transEvent(value) {
    const with_ppfx = ({ params }, rest) => <span><b>{`[${ params.player }] `}</b>{ rest }</span>

    // ice_reduce
    // almost all auto_event
    const events = {
      breathe_result: (v) => with_ppfx(v, "Respiration faite : +1 crâne")
    , choose_breathe_action: (v) => with_ppfx(v, "Action: respiration")
    , choose_move_pick_skull: (v) => with_ppfx(v, "Action: mouvement")
    , choose_aspiration: (v) => with_ppfx(v, "Action: aspiration")
    , loop_turn: (v) => "Nouveau tour de jeu ? Tour Pacoch ou plus aucun joueur en piste ?"
    , move_end: (v) => "Déplacement terminé"
    , move_made: (v) => "Nouveau déplacement"
    , phacoch_picked_skull_for_attack: (v) => with_ppfx(v, "Choix de crâne contre le phacoch : " + v.params.skulls)
    , sprint_end: () => false
    , sprint_resolve_split: () => "Sprint : départage"
    , sprint_split_loop: () => "Sprint : autre départage ?"
    , sprint_start: () => "Démarrage de la phase de sprint"
    , state_phacoch_attack_action: (v) => "Attaque du phacoch"
    , state_phacoch_kill: (v) => `Élimination : le Phacoch dévore le joueur ${v.params.player}`
    , state_race_end: (v) => "Fin du jeu"
    , state_surprise_end: (v) => "Fin de l'action carte surprise"
    , state_surprise_start: (v) => "Carte surprise"
    , state_turn_end: (v) => "Fin du tour de jeu (relève patineurs, phacoch)"
    , state_turn_phacoch_end: (v) => "Phacoch : fin du tour"
    , state_turn_phacoch_start: (v) => "Phacoch : début du tour"
    , state_turn_player_end: (v) => "Fin du tour du joueur"
    , state_turn_player_may_end: (v) => false // effet de case: tombe, surprise
    , state_turn_player_next: () => "Nouveau tour de joueur"
    , state_turn_start: () => "Nouveau tour"
    , trampoline_move: (v) => with_ppfx(v, `Trampoline ${v.params.score} cases de déplacement`)
    }

    // send_to_event
    const dice_suffix = (pfx, v) => `${pfx} : résultat dé => ${v.params.dice_rolls.join(',')}`

    const dice = {
      "attack_dice_rolled": (v) => dice_suffix("Attaque", v),
      "boomerang_dice_rolled": (v) => dice_suffix("Boomerang", v),
      "move_dice_roll_done": (v) => dice_suffix("Mouvement", v),
      // ne sait pas qui est qui
      "phacoch_attacker_dice_rolled": (v) => dice_suffix("Combat phacoch, résultat attaquant", v),
      "phacoch_defender_dice_rolled": (v) => dice_suffix("Combat phacoch, résultat défenseur", v),
      "sprint_split_dice_roll_done": (v) => dice_suffix("Départage sprint", v),
      // utilisé 2 fois
      "surprise_electricity_rolls": (v) => dice_suffix("Electricité", v),
      "surprise_roll_done": (v) => dice_suffix("Surprise", v),
    }

    const announce = {
      "announce_action_done": ({params}) => `Mouvement : annonce ${params.announce}`
    , "announce_boomerang_done": ({params}) => `Boomerang : annonce ${params.announce}`
    }

    const skulls = {
      "move_picked_skull": ({params}) => `Mouvement : crâne utilisé ${params.skulls}`
    , "phacoch_picked_skull_for_attack": ({params}) => `Attaque Phacoch : crâne utilisé ${params.skulls}`
    , "picked_skull_for_attack": ({params}) => `Attaque : crâne utilisé ${params.skulls}`
    }


    const d = {...events, ...dice, ...announce, ...skulls};

    console.log("GOT EVENT: ", value.e);

    return (d[value.e] || (() => false))(value)

  },
  trans(info) {
    return info.type == "state" ? this.transState(info.value) : this.transEvent(info.value);
  },
  addData({e, new_state}) {
    var xs = [];

    if (e) {
      xs.push({type: "event", value: e});
    }

    if (new_state && (Object.keys(new_state).length !== 0)) {
      xs.push({type: "state", value: new_state});
    }

    return xs;
  },
  render() {
    const {info} = this.state;

    return <div className="bar-game-stream">
      <ul> { info.map((v, i) => <li key={i}>{ v }</li>) } </ul>
    </div>
  }
})


/* ZOOM LINKS
https://github.com/d3/d3-zoom
https://github.com/d3/d3-zoom/issues/66 <=== HERE

https://bl.ocks.org/mbostock/6123708
https://bl.ocks.org/mrcslws/387a619d570508451ce2e7209ffaac7d
https://www.datamake.io/blog/d3-zoom-manual

https://stackoverflow.com/questions/38534500/d3-js-rewriting-zoom-example-in-version4
https://stackoverflow.com/questions/52094735/world-map-zoom-with-d3-reactjs
https://stackoverflow.com/questions/11808860/how-to-place-arrow-head-triangles-on-svg-lines
https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform
https://stackoverflow.com/questions/38224875/replacing-d3-transform-in-d3-v4
https://stackoverflow.com/questions/36887428/d3-event-is-null-in-a-reactjs-d3js-component

*/


const Ice = React.createClass({
  getInitialState() {
    return {};
  },
  componentDidMount() {
    const e = document.getElementById("zoom-target");
    const zoom_catcher = document.getElementById("zoom-catcher");

    const {width, height} = this.props.map;

    const tx = (window.innerWidth - width) / 2,
          ty = (window.innerHeight - height) / 2;
    zoom_catcher.setAttribute("transform", `translate(${tx}, ${ty})`);

    const zoom = d3.zoom()
        .extent([[0, 0], [width, height]])
        .scaleExtent([0.6, 5])
        .translateExtent([[0, 0], [width, height]]);

    zoom.on("zoom", () => {
      const t = d3.event.transform;
      e.setAttribute("transform", t.toString());
    });

    d3.select(zoom_catcher).call(zoom);

  },
  render() {
    const {game, map, info, ice_ctrl} = this.props;
    const {players, squares, positions, opts} = game;
    const {action} = game;

    const svg_map = <svg id="ice-svg" style={{width: "100%", height: "100%"}}>
        <defs>
          <marker id='head' orient="auto"
            markerWidth='10' markerHeight='20'
            refX='0.1' refY='2'>
            <path d='M0,0 V4 L2,2 Z' fill="red"/>
          </marker>
        </defs>

        <g id="zoom-catcher" width={map.width + "px"} height={map.height + "px"}>

          <g id="zoom-target">
            <image xlinkHref={map.url} width={map.width + "px"} height={map.height + "px"}  />

            <g id="positions">
              { positions && positions.map((p, i) => <Position key={p.id} position={p} game={game} />) }
            </g>
            <g id="squares">
              { !(positions && positions.length) && squares.map((s, i) => <Square key={s.id} square={s} game={game} />) }
            </g>
            <g id="squares_show_next">
              { opts.squares_show_next && <SquareLines squares={squares} /> }
            </g>
          </g>

        </g>
    </svg>


    return <div className="ice" >
      <div className="overlay">
        <div className="bar-action">
          { game.ctrl.action && game.ctrl.action() }
        </div>
        <div className="bar-info">
          <div style={{display: "none"}}> { /* debugger */ }
            <pre>{ JSON.stringify(game.state, null, 4) }</pre>
            <pre>{ JSON.stringify(game.players, null, 4) }</pre>
            { opts.recorder && <div className="recorder">
              <div className="button" onClick={() => opts.recorder.load()}>Charger</div>
              <div className="button" onClick={() => opts.recorder.save()}>Sauver</div>
            </div> }
          </div>
        </div>

        <GameOverview game={game} />

        <div className="bar-topleft">
          <GameCtrl ice_ctrl={ice_ctrl} />
          <GameStreamInfo info={info} />
        </div>
      </div>

      { svg_map }

    </div>
  }
})

export {
  Ice
}




import ReactDOM from 'react-dom';
const {Ice} = require('./ice_ui')
const {mind} = require('./ice_mind')
const {reduce, randomDiceRoll} = require('./ice_reduce')
const {scenarios} = require('./ice_scenario')

const recorder = require('./ice_recorder');

const {processSquares} = require('./ice_map');
const {loadMap} = require('./map_import');


require('../static/css/ice.css');
require('../static/css/normalize.css')
require('../static/css/font.css')


// qs_opts: map, nb_players, choose_dice, scenario
function ice(elem, qs_opts) {
  const map_name = qs_opts.map || "FightClub";

  loadMap(map_name, (map) => {
    ice_run(map, elem, qs_opts);
  })
}


function ice_run(map, elem, qs_opts) {
  const scenario = qs_opts.scenario || "normal";

  var ered;

  const nb_players = parseInt(qs_opts.nb_players)
  const slicePlayers = isNaN(nb_players) ? (xs) => xs : (xs) => xs.slice(0, nb_players);
  const dice_roll_manual = qs_opts.choosedice && qs_opts.choosedice != "0";

  const mayModify = (game) => {
    if (!dice_roll_manual && (game.state || {}).name == "dice_roll") {
      const auto = randomDiceRoll(game); // roll dice params
      return {...game, state: {...game.state, auto}};
    }
    return game;
  }

  const opts = {
    // squares_show: "rank",
    // squares_show: "id",
    // squares_show_next: true,
    // squares_show_special: true,
    // squares_show_effect: true,
    dice_roll_manual,
    recorder: {
      save: () => recorder.save(ered),
      load: (kind = "interval") => {
        var {game, events, event_players} = recorder.load()

        // function can't be saved: at least make recorder available
        game = {...game, opts: {...opts, recorder: opts.recorder}};

        // FIXME
        ered = createEventReducer(game, events, (new_game) => {
          ReactDOM.render(<Ice game={new_game} />, elem);
        });

        console.log(kind)
        event_players[kind](ered);
      }
    }
  }

  // TODO : scenario should specify a map by name
  const s = scenarios[scenario]()
  const squares = processSquares(map.squares || s.squares);
  const {width} = squares.find(s => s.special == "start");
  const radius_player = width/2 - 5;

  // set radius
  const players = slicePlayers(s.players.map(p => ({...p, r: radius_player})));
  const phacoch = {...s.phacoch, r: radius_player};

  // FIXME
  const game = {players: players, phacoch: phacoch, squares, positions: s.positions || [], turn: s.turn || [], opts};

  console.log(map);


  var last_render = {game: {...game, ctrl: {}}, info: {}};

  const ice_ctrl = gameCtrl(qs_opts);

  const renderGame = (new_game) => {
    last_render.game = new_game
    ReactDOM.render(<Ice game={new_game} map={map.img} info={last_render.info} ice_ctrl={ice_ctrl}  />, elem);
  }

  const renderInfo = (info) => {
    last_render.info = info
    ReactDOM.render(<Ice game={last_render.game} map={map.img} info={info} />, elem);
  }

  ered = createEventReducer(game, [], renderGame, renderInfo, mayModify)

  const events = s.events || [{e: "init", params: {players: players.map(p => p.id)}}];

  events.forEach(e => {
    ered.sendEvent(e)
  })

}



// encapsulate game var;
// pass events to continue the preivous historic
const createEventReducer = (game, events, render_up, sendInfo, mayModify) => {

  var game;

  const getGame = () => game;

  const render = (new_game) => {
    game = new_game;
    render_up(game)
  }

  // save game in between events (transient game modification happening in an action are removed)
  var prev_game = game;

  const disk = {game: game, events: events.slice()}
  const save = (e) => {
    disk.events.push(e)
  };
  const getGameSave = () => { return {...disk, events: disk.events.slice()}; }

  // FIXME: record mode, warning w/ events erasing each other or duplicating
  const sendEvent = (e, is_auto_event) => {

    // check event is allowed
    if (prev_game.state && !prev_game.state.events.includes(e.e)) {
      throw `${e.e} is not accepted by state events: ${prev_game.state.events.join(', ')}`
    }

    console.log("event: ", JSON.stringify(e));

    const new_game_reduced = reduce(prev_game, e);
    const new_game = mayModify(new_game_reduced);

    console.log("state: ", JSON.stringify(new_game.state));

    const auto_event = new_game.state.auto;

    // don't save auto_event, it will be reproduced by himself
    if (! is_auto_event) {
      save(e);
    }

    sendInfo({e: e, new_state: new_game.state})

    window.game = prev_game = game = new_game;

    if (auto_event) {
      const [ev] = new_game.state.events
      const new_event = {e: ev, params: auto_event === true ? {} : auto_event};

      sendEvent(new_event, true);
    } else {

      // we will be waiting for an event, look for an handler to provide it

      const handler = mind(game);
      const ctrl = handler.create(getGame, { render, sendEvent });
      game = {...game, ctrl}

      render(game);

      handler.exec && handler.exec(ctrl, game);
    }
  };

  return { sendEvent, getGameSave }
}

function gameCtrl(qs_opts) {
  console.log("qs opts: ", qs_opts);

  const changeQS = (qs_d) => {
    console.log(qs_d);
    const qs = Object.keys(qs_d).map(k => `${k}=${qs_d[k]}`).join("&");
    location.search = "?" + qs;
  }

  return {
    changeMap: (map) => {
      changeQS({...qs_opts, map})
    }
  }
}

export {
  ice
}


// gen_range of length len
const gen_range = (len) => {
  var l = [];
  for(var i = 0; i < len; i++) {
    l.push(i)
  }
  return l;
}


const imgFigToPlayer = (p) => {
  const id = p.split(".")[0];
  return {url: `res/figs/${p}`, id};
}

// all squares
const players_fig = [
  "3D.png", "Freddy.png", "Hobbitus.png", "MarcAngel.png", "Maurice.png",
  "Mesrine.png", "Moos.png", "Odin.png", "Skeletor.png", "Snake.png",
].map(imgFigToPlayer);

// all squares
// Wolfy, Ogrimax, Orcus
const phacoch_fig = imgFigToPlayer("Wolfy.png")


const getPosition = () => {
  const positions = gen_range(30).map(i => {
    const nb = i + 1;

    const idx_ui = 29 - i;

    return {
      id: "" + nb, rank: nb, players: [],
      x: 100 + (100 * (i % 6)),
      y: 100 + (150 * Math.floor(idx_ui / 6))
    }
  })

  return positions.map((s) => {
    const {x, y} = s;
    const height = 100, width = 100;
    const center = {x: x + height/2, y: y + width/2};
    return {...s, height, width, x, y, center};
  })
}

// une seule case départ: *tout* est calculé depuis et check: suffit de voir si on passe neg à pos
// plusieurs case fin: check plus facile
const getSimpleSquares = () => {
  return [{
    id: 1, x: 100, y: 100, type: "flat", next: [2]
  }, {
    id: 2, x: 200, y: 100, type: "flat", next: [3]
  }, {
    id: 3, x: 300, y: 100, type: "decrease", next: [4]
  }, {
    id: 4, x: 300, y: 200, type: "decrease", next: [5]
  }, {
    id: 5, x: 300, y: 300, type: "decrease", next: [6]
  }, {
    id: 6, x: 300, y: 400, type: "decrease", next: [7]
  }, {
    id: 7, x: 400, y: 400, type: "increase", next: [8]
  }, {
    id: 8, x: 500, y: 400, type: "increase", next: [9]
  }, {
    id: 9, x: 600, y: 400, type: "increase", next: [10]
  }, {
    id: 10, x: 700, y: 400, type: "increase", next: [1]
  }]
}

const getBetterSquares = () => {
  return [{
    id: 1, x: 100, y: 100, type: "flat", next: [2,3]
  }, {
    id: 2, x: 150, y: 0, type: "flat", next: [3]
  }, {
    id: 3, x: 200, y: 100, type: "flat", next: [4], special: "start"
  }, {
    id: 4, x: 300, y: 100, type: "flat", next: [5], effect: "surprise"
  }, {
    id: 5, x: 300, y: 200, type: "trampoline", next: [6]
    // id: 5, x: 300, y: 200, type: "decrease", next: [6, 13]
  // }, {
  //   id: 13, x: 200, y: 250, type: "decrease", next: [6, 12]
  }, {
    id: 6, x: 300, y: 300, type: "decrease", next: [7, 12], special: "sprint"
  }, {
    id: 12, x: 400, y: 350, type: "decrease", next: [7, 15], special: "sprint"
  }, {
    id: 15, x: 400, y: 500, type: "decrease", next: [7, 16], special: "end"
  }, {
    id: 16, x: 250, y: 500, type: "decrease", next: [8]
  }, {
    id: 7, x: 300, y: 400, type: "decrease", next: [8, 16], special: "end", effect: "hole"
  }, {
    id: 8, x: 200, y: 400, type: "increase", next: [9]
  }, {
    id: 9, x: 100, y: 400, type: "increase", next: [10]
  }, {
    id: 10, x: 0, y: 400, type: "increase", next: [11]
  }, {
    id: 11, x: 0, y: 300, type: "increase", next: [13], effect: "hurdle"
  }, {
    id: 13, x: 0, y: 200, type: "increase", next: [14]
  }, {
    id: 14, x: 0, y: 100, type: "flat", next: [1]
  }]
}


const getPlayers = () => {
  return [{
    id: "Col",
    status: "ok",
    race_turn: -1,
    skulls: 1,
    color: "slategray",
    selected: false,
    items: [],
  }, {
    id: "Docteur Crabe",
    status: "ok",
    race_turn: -1,
    skulls: 0,
    items: [],
    color: "fuchsia",
    selected: false
  }, {
    id: "Mike Angel",
    status: "ok",
    race_turn: -1,
    skulls: 3,
    items: [],
    color: "gold",
    selected: false
  }];
}

const getPhacoch = () => {
  return {
    status: "ok",
    color: "red",
    selected: false,
    move: 5,
    race_turn: -1
  }
}

const scenarios = {

  normal() {
    const squares = getBetterSquares();

    const players = players_fig.slice(0, 2).map(x => ({...x, skulls: 0}));
    const phacoch = phacoch_fig;

    return { squares, players, phacoch }
  },

  // and fall?
  jump() {
    const squares = getBetterSquares();
    const [p1, p2, p3] = getPlayers();
    const phacoch = getPhacoch();

    const players = [
      p1,
      {...p2, status: "ko"},
      p3
    ];


    squares[4].player = p1.id;
    squares[5].player = p2.id;
    squares[1].player = p3.id;

    return { squares, players, phacoch }
  },

  attack() {
    const squares = getBetterSquares();
    const [p1, p2] = getPlayers();
    const phacoch = getPhacoch();

    const players = [p1, p2];

    squares[4].player = p1.id;
    squares[5].player = p2.id;
    return { squares, players, phacoch }
  },

  boomerang() {
    const squares = getBetterSquares();
    const [p1, p2] = getPlayers();
    const phacoch = getPhacoch();

    const players = [p1, p2];

    squares[3].player = p1.id;
    squares[5].player = p2.id;

    const events = [
      { "e": "init" },
      { "e": "choose_pick_skull", "params": { "player": "Col" } },
      { "e": "picked_skull", "params": { "player": "Col", "skulls": 0 } },
      { "e": "announce_action_done", "params": { "player": "Col", "nb_dice": 1, "announce": 2 } },
      { "e": "dice_roll_done", "params": { "player": "Col", "dice_rolls": [ 4 ], "announce": 2 } },
      { "e": "move_made", "params": { "path_used": [ 5 ], "player": "Col" } },
      { "e": "move_end", "params": { "player": "Col" } },
      { "e": "choose_attack", "params": { "player": "Col" } },
      { "e": "attack_target_chosen", "params": { "attacker": "Col", "defender": "Docteur Crabe" } },
      { "e": "attack_type_chosen", "params": { "player": "Col", "attack_type": "normal" } },
      { "e": "picked_skull_for_attack", "params": { "player": "Col", "skulls": 0 } },
      { "e": "picked_skull_for_attack", "params": { "player": "Docteur Crabe", "skulls": 0 } },
      { "e": "attack_dice_rolled", "params": { "player": "Col", "dice_rolls": [ 11 ] } }
    ];

    return { squares, players, events, phacoch }
  },

  aspi() {
    const squares = getBetterSquares();
    const [p1, p2] = getPlayers();
    const phacoch = getPhacoch();

    const players = [p1, p2];

    squares[3].player = p1.id;
    squares[2].player = p2.id;

    const events = [
      { "e": "init" },
      { "e": "choose_pick_skull", "params": { "player": "Col" } },
      { "e": "picked_skull", "params": { "player": "Col", "skulls": 0 } },
      { "e": "announce_action_done", "params": { "player": "Col", "nb_dice": 1, "announce": 2 } },
      { "e": "dice_roll_done", "params": { "player": "Col", "dice_rolls": [ 4 ], "announce": 2 } },
      { "e": "move_made", "params": { "path_used": [ 5, 6, 7, 8 ], "player": "Col" } },
      { "e": "move_end", "params": { "player": "Col" } },
    ];

    // order square/player
    // ordre du tour: nécessaire pour l'aspiration
    // nécessaire aussi (au moins la liste des joueurs calculé) pour les faire jouer les uns après les autres?!
    // où stocker le statut du jour : ko etc. ? sur le joueur ?
    // sinon { squares, players, turn: {}, state: (transient)}
    // turn : list {player: , current_square: id, square_beginning_of_turn: id, player_status: ko, etc.}
    // in_sprint? 
    const turn = squares.filter(x => x.player).map(({id, player}) => {
      // aggregate the move he made during the turn
      // the path must also contains any info that may be transient
      // (if any player were here and their status)
      // if player is reachable!
      // if it attacked and so on... necessary for aspiration
      return {player, start_square: id, moves: []};
    }).reverse();

    return { squares, players, turn, events, phacoch }
  },



  position: () => {
    const players = getPlayers();
    const squares = getBetterSquares();
    const phacoch = getPhacoch();
    const events = [{ "e": "init_position" }];
    const positions = getPosition()

    return {squares, players, positions, events, phacoch};
  },

  sprint() {
    const events = [
      {"e":"init","params":{"players":["Col","Docteur Crabe","Mike Angel"]}},
      {"e":"state_turn_start"},
      {"e":"loop_turn"},
      {"e":"state_turn_player_next"},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[3],"announce":3}},
      {"e":"move_made","params":{"path_used":[4,5,6,7],"player":"Col"}},
      {"e":"move_end","params":{"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"state_turn_player_end"},
      {"e":"loop_turn"},
      {"e":"state_turn_player_next"},
      {"e":"choose_pick_skull","params":{"player":"Docteur Crabe"}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Docteur Crabe","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Docteur Crabe","dice_rolls":[3],"announce":3}},
      {"e":"move_made","params":{"path_used":[3,4,5,6,12],"player":"Docteur Crabe"}},
      {"e":"move_end","params":{"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"state_turn_player_end"},
      {"e":"loop_turn"},
      {"e":"state_turn_player_next"},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[3],"announce":3}},
    ];

    const players = getPlayers();
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, events, phacoch};
  },


  sprint_echappee() {
    const players = getPlayers().slice(0, 2);
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, phacoch};
  },


  end() {
    const events = [
      {"e":"init","params":{"players":["Col","Docteur Crabe","Mike Angel"]}},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[5]}},
      {"e":"move_made","params":{"path_used":[4,5,6,7,8,9,10,11],"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"choose_pick_skull","params":{"player":"Docteur Crabe"}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Docteur Crabe","announce":4}},
      {"e":"dice_roll_done","params":{"player":"Docteur Crabe","dice_rolls":[4]}},
      {"e":"move_made","params":{"path_used":[3,4,5,6,7,8,9,10],"player":"Docteur Crabe"}},
      {"e":"move_end","params":{"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":4}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[4]}},
      {"e":"move_made","params":{"path_used":[3,4,5,6,7,8,9],"player":"Mike Angel"}},
      {"e":"move_end","params":{"player":"Mike Angel"}},
      {"e":"pass_attack","params":{"player":"Mike Angel"}},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":5}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[5]}},
      {"e":"move_made","params":{"path_used":[13,14,1,3],"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"choose_pick_skull","params":{"player":"Docteur Crabe"}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Docteur Crabe","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Docteur Crabe","dice_rolls":[3]}},
      {"e":"move_made","params":{"path_used":[11,13],"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":3}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[3]}},
      {"e":"move_made","params":{"path_used":[10,11],"player":"Mike Angel"}},
      {"e":"pass_attack","params":{"player":"Mike Angel"}},
    ];

    const players = getPlayers();
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, events, phacoch};
  },

  end_game() {
    const events = [
      {"e":"init","params":{"players":["Col","Docteur Crabe","Mike Angel"]}},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[5]}},
      {"e":"move_made","params":{"path_used":[4,5,6,7,8,9,10,11],"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"choose_pick_skull","params":{"player":"Docteur Crabe"}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Docteur Crabe","announce":4}},
      {"e":"dice_roll_done","params":{"player":"Docteur Crabe","dice_rolls":[4]}},
      {"e":"move_made","params":{"path_used":[3,4,5,6,7,8,9,10],"player":"Docteur Crabe"}},
      {"e":"move_end","params":{"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":4}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[4]}},
      {"e":"move_made","params":{"path_used":[3,4,5,6,7,8,9],"player":"Mike Angel"}},
      {"e":"move_end","params":{"player":"Mike Angel"}},
      {"e":"pass_attack","params":{"player":"Mike Angel"}},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":5}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[5]}},
      {"e":"move_made","params":{"path_used":[13,14,1,3],"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"choose_pick_skull","params":{"player":"Docteur Crabe"}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"picked_skull","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Docteur Crabe","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Docteur Crabe","dice_rolls":[3]}},
      {"e":"move_made","params":{"path_used":[11,13],"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"pass_attack","params":{"player":"Docteur Crabe"}},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":3}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[3]}},
      {"e":"move_made","params":{"path_used":[10,11],"player":"Mike Angel"}},
      {"e":"pass_attack","params":{"player":"Mike Angel"}},
      {"e":"choose_pick_skull","params":{"player":"Col"}},
      {"e":"picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":3}},
      {"e":"dice_roll_done","params":{"player":"Col","dice_rolls":[3]}},
      {"e":"move_made","params":{"path_used":[4,5,6,7,8],"player":"Col"}},
      {"e":"choose_breathe_action","params":{"player":"Docteur Crabe"}},
      {"e":"breathe_result","params":{"player":"Docteur Crabe","square":14}},
      {"e":"choose_pick_skull","params":{"player":"Mike Angel"}},
      {"e":"picked_skull","params":{"player":"Mike Angel","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Mike Angel","announce":5}},
      {"e":"dice_roll_done","params":{"player":"Mike Angel","dice_rolls":[6]}},
      {"e":"move_made","params":{"path_used":[13],"player":"Mike Angel"}},
      {"e":"move_end","params":{"player":"Mike Angel"}},
      {"e":"choose_attack","params":{"player":"Mike Angel"}},
      {"e":"attack_target_chosen","params":{"attacker":"Mike Angel","defender":"Docteur Crabe"}},
      {"e":"attack_type_chosen","params":{"player":"Mike Angel","attack_type":"switch"}},
      {"e":"picked_skull_for_attack","params":{"player":"Mike Angel","skulls":0}},
      {"e":"picked_skull_for_attack","params":{"player":"Docteur Crabe","skulls":0}},
      {"e":"attack_dice_rolled","params":{"player":"Mike Angel","dice_rolls":[12]}},
      {"e":"announce_boomerang_done","params":{"player":"Docteur Crabe","announce":4}},
      {"e":"boomerang_dice_rolled","params":{"player":"Docteur Crabe","dice_rolls":[8]}},
    ];

    const players = getPlayers();
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, events, phacoch};
  },
  phacoch() {
    const events = [
      {"e":"init","params":{"players":["Col"]}},
    ];

    const players = getPlayers().filter(p => p.id == "Col");
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, events, phacoch};
  },

  trampo() {
    const events = [
      {"e":"init","params":{"players":["Col","Docteur Crabe","Mike Angel"]}},
      {"e":"choose_move_pick_skull","params":{"player":"Col"}},
      {"e":"move_picked_skull","params":{"player":"Col","skulls":0}},
      {"e":"announce_action_done","params":{"player":"Col","announce":3}},
      {"e":"move_dice_roll_done","params":{"player":"Col","dice_rolls":[2]}},
      {"e":"move_made","params":{"path_used":[4,5],"player":"Col"}},
      {"e":"pass_attack","params":{"player":"Col"}},
      {"e":"choose_breathe_action","params":{"player":"Docteur Crabe"}},
      {"e":"breathe_result","params":{"player":"Docteur Crabe","square":3}},
      {"e":"choose_breathe_action","params":{"player":"Mike Angel"}},
      {"e":"breathe_result","params":{"player":"Mike Angel","square":2}},
    ]

    const players = getPlayers();
    const squares = getBetterSquares();
    const phacoch = getPhacoch();

    return {squares, players, events, phacoch};
  }
};


export {
  scenarios
}


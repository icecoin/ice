import React from 'react';

const {getNextSquares, getAllShortestPath, getPathSquares, prepareSquare} = require('./ice_rules');
const {player_force_select, player_select, square_select, move_to, mark_choose_square} = require('./ice_mind_shared');

const position = require('./ice_position');

const { getInFlightSquare } = require('./ice_reduce');


// gen_range of length len
const gen_range = (len) => {
  var l = [];
  for(var i = 0; i < len; i++) { l.push(i) }
  return l;
}


const defaultCtrl = () => {
  const cb = () => false
  return {
    player: { onClick: cb },
    square: { onClick: cb, onMouseOver: cb, onMouseOut: cb },
  };
}


var state_handlers = [];


const transformAlongCircle = (nb, radius, angle_circle, angle_start) => {
  // radius should be in em, px, etc.
  const section_nb = angle_circle == 360 ? nb : nb - 1;
  const angle_degree = angle_circle / section_nb;

  var angle = angle_start, l = [];

  for (var i = 0; i < nb; i++) {
    l.push(`rotate(${angle}deg) translate(${radius}) rotate(-${angle}deg)`)
    angle = angle + angle_degree;
  }

  return l;
}

const circleStyles = (nb_items) => {
  const nb2radius = {
    4: 4, 6: 7, 8: 9, // dice (has 12)
    3: 3, 5: 6, 7: 8 // announce (has 11 and 12 also)
  }
  const angle_circle = 150, angle_start = 195;
  // const angle_circle = 180, angle_start = 180;

  var transform;
  if (nb_items < 11) {
    transform = transformAlongCircle(nb_items, nb2radius[nb_items] + "em", angle_circle, angle_start);
  } else {
    const [outer, inner] = nb_items == 12 ? [8, 4] : [7, 4];

    const transform_outer = transformAlongCircle(outer, nb2radius[outer] + "em", angle_circle, angle_start);
    const transform_inner = transformAlongCircle(inner, nb2radius[inner] + "em", angle_circle, angle_start);
    transform = transform_outer.concat(transform_inner);
  }

  const color_range = [
    "#0095E5", "#0163E7", "#0330E9", "#0D05EC", "#4406EE", "#7B08F0",
    "#B30AF3", "#EB0CF5", "#F70ECC", "#FA1098", "#FC1264", "#FF1430",
  ];

  return gen_range(nb_items).map(i => {
    const jump = 12 / nb_items;
    return {
      transform: transform[i],
      backgroundColor: color_range[Math.round(i * jump)]
    };
  })
}




const end_race_action = {
  name: "end_race_action",
  handle: ({name}) => { return name == "end_race_action"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const action = () => {
      return <div>
        <div className="state-title">Fin du jeu !</div>
      </div>
    }

    return {...ctrl, action}
  }
};

const choose_action = {
  name: "choose_action",
  handle: ({name}) => { return name == "choose_action"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const {state} = getGame();
    const {params: {player}, events} = state;

    const choices = [{
      active: events.includes("choose_breathe_action"),
      label: "Respiration",
      onClick: () => {
        sendEvent({
          e: "choose_breathe_action",
          params: { player }
        })
      }
    }, {
      active: events.includes("choose_move_pick_skull"),
      label: "Mouvement",
      onClick: () => {
        sendEvent({
          e: "choose_move_pick_skull",
          params: { player }
        })
      }
    }, {
      active: events.includes("choose_aspiration"),
      label: "Aspiration",
      onClick: () => {
        sendEvent({
          e: "choose_aspiration",
          params: { player }
        })
      }
    }, {
      active: events.includes("choose_attack"),
      label: "Attaque",
      onClick: () => {
        sendEvent({
          e: "choose_attack",
          params: { player }
        })
      }
    }].filter(x => x.active)

    const action = () => {
      return <div>
        <div className="choice-list">
          { choices.map((c,i) => <div className="choice-item" key={i} onClick={c.onClick}>{ c.label }</div>) }
        </div>
        <div className="state-title">[{player}] Choix du tour de jeu</div>
      </div>
    }

    return {...ctrl, action}
  }
}

const may_attack_action = {
  name: "may_attack_action",
  handle: ({name}) => { return name == "may_attack_action"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const {player} = getGame().state.params;

    const choices = [{
      label: "Attaque",
      onClick: () => sendEvent({ e: "choose_attack_after_move", params: { player } })
    }, {
      label: "Passer",
      onClick: () => sendEvent({ e: "pass_attack", params: { player } })
    }];

    const action = () => {
      return <div>
        <div className="choice-list">
          { choices.map((c,i) => <div className="choice-item" key={i} onClick={c.onClick}>{ c.label }</div>) }
        </div>
        <div className="state-title">[{player}] Décision de lancer une attaque</div>
      </div>
    }

    return {...ctrl, action}
  }
}

const breathe_action = {
  name: "breathe_action",
  handle: ({name}) => { return name == "breathe_action"; },
  create: (getGame, {render, sendEvent}) => {
    const {player} = getGame().state.params

    const moveNextState = (square) => {
      sendEvent({
        e: "breathe_result",
        params: { player, square }
      })
    }

    var ctrl = defaultCtrl();

    return {...ctrl,
      // player: {onClick:  player_select(getGame, player, reachable_squares, render) },
      render,
      square: {
        onClick: square_select(getGame, ({players, squares, path_used}) => {
          const [ square ] = getPathSquares(path_used);

          moveNextState(square)
        }),
        onMouseOver: () => false,
        onMouseOut: () => false
      },
      action: () => {
        return <div>
        <div className="state-title">[{player}] Respiration</div>
        </div>
      }
    }
  },
  exec(ctrl, game) {
    const {player, reachable_squares} = game.state.params;
    const {players, squares} = player_force_select(game, player, reachable_squares);

    ctrl.render({...game, players, squares});
  }
};

const pick_skull_action = {
  name: "pick_skull_action",
  handle: ({name}) => { return name == "pick_skull_action"; },
  create: (getGame, {render, sendEvent}) => {
    const {params, events: [send_to_event] } = getGame().state;
    const {player, type} = params;
    const {skulls} = getGame().players.find(p => p.id == player)

    var ctrl = defaultCtrl();

    const menuClick = (r) => {
      sendEvent({
        e: send_to_event,
        params: {player, skulls: r}
      });
    }

    const range = gen_range(skulls + 1)

    const SkullCtrl = React.createClass({
      getInitialState() {
        return {choice: 0, stock: skulls};
      },
      submit() {
        menuClick(this.state.choice)
      },
      inc() {
        this.setState({
          choice: Math.min(this.state.choice + 1, skulls),
          stock: Math.max(this.state.stock - 1, 0)
        })
      },
      dec() {
        this.setState({
          choice: Math.max(this.state.choice - 1, 0),
          stock: Math.min(this.state.stock + 1, skulls)
        })
      },
      render() {
        return <div className={`action-pick-skull action-pick-skull-${skulls}`}>
          <div className="choice-skull">
            <div className="choice-skull-count-stock">
              <span className="choice-skull-qty">{ this.state.stock }</span>
              { /* <span class="choice-skull-label">Stock</span> */ }
            </div>

            <div className="choice-skull-ctrl">
              <div className="choice-skull-dec" onClick={this.dec}></div>
              <div className="choice-skull-submit" onClick={this.submit}></div>
              <div className="choice-skull-inc" onClick={this.inc}></div>
            </div>

            <div className="choice-skull-count-used">
              <span className="choice-skull-qty">{ this.state.choice }</span>
              { /* <span class="choice-skull-label">À utiliser</span> */ }
            </div>

          </div>

          { false && <div className="choice-list">
            { range.map((r,i) => <div className="choice-item" key={i} onClick={() => menuClick(r)}>{ r }</div>) }
          </div> }

        <div className="state-title">[{player}] Utilisation de crânes</div>
        </div>
      }
    })

    return {...ctrl, action: () => <SkullCtrl />}
  }
}




const announce_action = {
  name: "announce_action",
  handle: ({name}) => { return name == "announce_action"; },
  create: (getGame, {render, sendEvent}) => {
    const {params, events: [send_to_event] } = getGame().state;
    const {player, nb_dice, dice} = params;

    const is_boomerang = send_to_event == "announce_boomerang_done";

    // should be
    const announceChoice = (dice, is_boomerang) => {
      if (is_boomerang) {
        return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      }

      return {
         4: [2, 3, 4],
         6: [2, 3, 4, 5, 6],
         8: [2, 3, 4, 5, 6, 7, 8],
        12: [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
      }[dice]
    }

    const range = announceChoice(dice, is_boomerang);

    var ctrl = defaultCtrl();

    const menuClick = (r) => {
      sendEvent({
        e: send_to_event,
        params: {player, announce: r}
      })
    }

    const klass = is_boomerang ? "action-announce-boomerang" : `action-announce-${dice}`;

    const items = circleStyles(range.length);

    return {...ctrl,

      action: () => {
        return <div className={`action-announce ${klass}`}>
          <div className="choice-list">
            { items.map((style,i) => {
                const r = range[i];
                return <div className="choice-item" key={i} style={style} onClick={() => menuClick(r)}>
                  <span>{ r }</span>
                </div>
              })
            }
          </div>
          <div className="state-title">[{player}] Annonce {is_boomerang ? "Boomerang" : "mouvement"}</div>
        </div>
      }
    }
  }
};

const dice_roll = {
  name: "dice_roll",
  handle: ({name}) => { return name == "dice_roll"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const {state, opts} = getGame();
    const {params, events: [send_to_event]} = state;
    // we may not always have a player, can be phacoch
    var {player, nb_dice, dice} = params;


    const goNext = (dice_rolls) => {
      sendEvent({ e: send_to_event, params: {player, dice_rolls} })
    }

    const items = circleStyles(dice);

    const action = () => {
      return <div className={`action-dice action-dice-${dice}`}>
        <div className="choice-list">
          { items.map((style, i) => {
              return <div className="choice-item" key={i} style={style} onClick={() => goNext([i + 1])}>
                <span>{ i + 1 }</span>
              </div>
            })
          }
        </div>
        <div className="state-title">[{player}] Jet de dé {dice}</div>
      </div>
    }

    return {...ctrl, goNext, action};
  }
}


const move_ctrl = {
  name: "moving",
  handle: ({name}) => { return name == "moving"; },
  // will be triggered by UI
  create: (getGame, {render, sendEvent}) => {
    const game = getGame();
    const {events} = game.state;
    const {score, player} = game.state.params

    var player_square = getInFlightSquare(getGame().squares, player);

    var best_path_by_end = getAllShortestPath(prepareSquare(game), player_square.id, score)

    const has_move_end = events.indexOf("move_end") !== -1;

    const endMoveClick = () => {
      sendEvent({
        e: "move_end",
        params: { player }
      })
    }

    const action = () => {
      return <div>
        <div className="state-info">
        Reste {score} case(s) de déplacement
        </div>
        { has_move_end && <div className="choice-list">
          <div className="choice-item" onClick={endMoveClick}>Terminer</div>
        </div> }
        <div className="state-title">[{player}] Déplacement</div>
      </div>
    }

    const move_to_square = move_to(getGame, best_path_by_end, ({players, squares, path_used}) => {
      path_used = getPathSquares(path_used);

      sendEvent({
        e: "move_made",
        params: {path_used, player}
      })
    })

    return {
      action,
      render,
      player: {
        onClick: (id) => {
          const player_square = game.squares.find(s => s.player == id);
          move_to_square(player_square);
        }
      },
      // more than a square, it's a place
      square: {
        onClick: move_to_square,
        onMouseOver(id) {
          const game = getGame()

          const {path} = best_path_by_end[id] || {};

          // show route (except last square)
          if (path) {
            const p = getPathSquares(path.slice(0, path.length - 1));

            const squares = game.squares.map(s => {
              return {...s, in_move_path: p.indexOf(s.id) == -1 ? null : player}
            })

            render({...game, squares})
          }
        },
        onMouseOut(id) {
          const game = getGame();

          const squares = game.squares.map(s => {
            return {...s, in_move_path: null};
          })
          render({...game, squares})
        }
      }
    };
  },
  exec(ctrl, game) {
    const {player, score} = game.state.params;
    const player_square = getInFlightSquare(game.squares, player);

    const reachable_squares = getNextSquares(prepareSquare(game), player_square.id, score);
    const {players, squares} = player_force_select(game, player, reachable_squares);


    ctrl.render({...game, players, squares});
  }
}


const attack_action = {
  name: "attack_action",
  handle: ({name}) => { return name == "attack_action"; },
  create: function(getGame, {render, sendEvent}) {
    const {player, attackable_squares} = getGame().state.params

    // TODO: NO ACTION?
    var ctrl = defaultCtrl();

    return {...ctrl,
      player: {
        onClick: player_select(getGame, player, attackable_squares, render)
      },
      square: {
        onClick: (id) => {
          const s = attackable_squares.find(s => s.id == id)
          if (!s) return;

          sendEvent({
            e: "attack_target_chosen",
            params: {
              attacker: player,
              defender: s.player || s.phacoch
            }
          })
        },
        onMouseOver: () => false,
        onMouseOut: () => false
      },
    };
  }
};


const choose_attack_type = {
  name: "choose_attack_type",
  handle: ({name}) => { return name == "choose_attack_type"; },
  create: function(getGame, {render, sendEvent}) {
    const {player} = getGame().state.params

    const res = (attack_type) => {
      sendEvent({
        e: "attack_type_chosen",
        params: {player, attack_type}
      })
    }

    const action = () => {
      return <div>
        <div className="choice-list">
          <div className="choice-item" onClick={() => res("normal")}>Normal</div>
          <div className="choice-item" onClick={() => res("switch")}>-1 pour prendre la place</div>
        </div>
        <div className="state-title">[{player}] Choix du type attaque</div>
      </div>
    }

    var ctrl = defaultCtrl();

    return {...ctrl, action}
  }
}


// inspired by attack_action
const choose_aspiration_player = {
  name: "choose_aspiration_player",
  handle: ({name}) => { return name == "choose_aspiration_player"; },
  // will be triggered by UI
  create: function(getGame, {render, sendEvent}) {
    const {player, aspirable_squares} = getGame().state.params
    var ctrl = defaultCtrl();

    // TODO: NO ACTION?
    return {...ctrl,
      render,
      square: {
        onClick: (id) => {
          const s = aspirable_squares.find(s => s.id == id)
          if (!s) return;

          sendEvent({
            e: "aspiration_target_chosen",
            params: {
              followed: s.player
            }
          })
        },
        onMouseOver: () => false,
        onMouseOut: () => false
      },
    };
  },
  exec(ctrl, game) {
    const {player} = game.state.params;
    const player_square = getInFlightSquare(game.squares, player);

    // TODO: show what you can aspire
    const {players, _squares} = player_force_select(game, player, []);

    ctrl.render({...game, players });
  }
};


const ask_accept_aspiration = {
  name: "ask_accept_aspiration",
  handle: ({name}) => { return name == "ask_accept_aspiration"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();
    const {player, follower} = getGame().state.params;

    const allow = () => sendEvent({ e: "aspiration_allow" });
    const deny = () => sendEvent({ e: "aspiration_deny" });

    const action = () => {
      return <div>
        <div className="choice-list">
          <div className="choice-item" onClick={allow}>Accepter l'aspiration</div>
          <div className="choice-item" onClick={deny}>Refuser l'aspiration</div>
        </div>
        <div className="state-title">[{player}] Demande d'aspiration de {follower}</div>
      </div>
    }

    return {...ctrl, action}
  }
}

const aspiration_challenge = {
  name: "aspiration_challenge",
  handle: ({name}) => ["aspiration_challenge_follower", "aspiration_challenge_followed"].indexOf(name) !== -1,
  create: (getGame, {render, sendEvent}) => {
    const {player} = getGame().state.params;
    const {events: [e]} = getGame().state;

    const is_follower = getGame().state.name == "aspiration_challenge_follower";

    const sendCard = (card) => {
      sendEvent({ e, params: { card } });
    }

    const aspi_choice = [["power", "Puissance"], ["reflex", "Réflexe"], ["cunning", "Astuce"]];


    const action = () => {
      return <div>
        <div className="choice-list">
          { aspi_choice.map(([k, label], i) => {
              return <div key={i} className="choice-item" onClick={() => sendCard(k)}>{ label }</div>
            })
          }
        </div>
        <div className="state-title">[{player}] Aspiration : défi du {is_follower ? "suiveur" : "meneur"}</div>
      </div>
    }

    const ctrl = defaultCtrl();
    return {...ctrl, action}
  }
}


const choose_aspiration_square = {
  name: "choose_aspiration_square",
  handle: ({name}) => { return name == "choose_aspiration_square"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();
    const game = getGame();

    const {player, follower, followed, choosable_squares} = game.state.params;

    const action = () => {
      return <div>
        <div className="state-title">Joueur {player} : choix de la case de fin d'aspi</div>
      </div>
    }

    return {...ctrl,
      render,
      square: {
        onClick: (id) => {
          if (! choosable_squares.includes(id)) return;

          sendEvent({
            e: "aspiration_square_chosen",
            params: { square: id }
          })
        },
        onMouseOver(id) {
          if (! choosable_squares.includes(id)) return;

          const game = getGame();
          const squares = game.squares.map(s => {
            return {...s, in_move_path: s.id == id ? follower : null}
          })
          render({...game, squares})
        },
        onMouseOut(id) {
          if (! choosable_squares.includes(id)) return;

          const game = getGame();
          const squares = game.squares.map(s => {
            return s.in_move_path ? {...s, in_move_path: null} : s;
          })
          render({...game, squares})
        }
      },
    };
  },
  exec(ctrl, game) {
    const {  choosable_squares } = game.state.params;
    const squares = mark_choose_square(game, game.state.params.follower, choosable_squares);
    ctrl.render({...game, squares});
  }
}

const may_reattack_action = {
  name: "may_reaattack_action",
  handle: ({name}) => { return name == "may_reattack_action"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const {phacoch_defending, player} = getGame().state.params

    const reattack_continue = () => {
      sendEvent({ e: phacoch_defending ? "phacoch_reattack_continue" : "reattack_continue" });
    }
    const reattack_cancel = () => {
      sendEvent({ e: phacoch_defending ? "phacoch_reattack_cancel" : "reattack_cancel" });
    }

    const action = () => {
      return <div>
        <div className="choice-list">
          <div className="choice-item" onClick={reattack_continue}>Oui, réattaquer</div>
          <div className="choice-item" onClick={reattack_cancel}>Non, fin du tour</div>
        </div>
        <div className="state-title">[{player}] Décision de réattaquer</div>
      </div>
    }

    return {...ctrl, action}
  }
};


// surprise

const surprise_phacoch_move = {
  name: "surprise_phacoch_move",
  handle: ({name}) => { return name == "surprise_phacoch_move"; },
  create: (getGame, {render, sendEvent}) => {
    var ctrl = defaultCtrl();

    const phacoch_move = (forward) => {
      sendEvent({ e: "surprise_phacoch_move_chosen", params: {forward}})
    }

    const action = () => {
      return <div>
        <div className="choice-list">
          <div className="choice-item" onClick={() => phacoch_move(true)}>Avancer le phacoch</div>
          <div className="choice-item" onClick={() => phacoch_move(false)}>Reculer le phacoch</div>
        </div>
        <div className="state-title">Phacoch avancer/reculer</div>
      </div>
    }

    return {...ctrl, action}
  }
};




const handlers = [
  end_race_action,
  choose_action,
  breathe_action,
  pick_skull_action,
  announce_action,
  dice_roll,
  move_ctrl, // state spec: todo
  attack_action,
  may_attack_action,
  choose_attack_type,
  choose_aspiration_player,
  ask_accept_aspiration,
  aspiration_challenge,
  choose_aspiration_square,
  may_reattack_action, // state spec: can be improved
  surprise_phacoch_move,
];

const position_handlers = position.getHandlers();

handlers.concat(position_handlers).forEach(h => {
  state_handlers.push(h)
})




const mind = (game) => {
  return state_handlers.find(s => s.handle(game.state));
}


export { mind }

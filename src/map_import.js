
const ajax_get = (url, cb_ok, cb_err) => {
  const req = new XMLHttpRequest();
  req.open('GET', url);
  req.onload = () => req.status === 200 ? cb_ok(req.response) : cb_err(Error(req.statusText));
  req.onerror = (e) => cb_err(Error(`Network Error: ${e}`));
  req.send();
}

const loadMap = (name, cb) => {
  const img_url = `res/maps/${name}.jpg`;
  const xml_url = `res/maps/${name}.xml`;

  ajax_get(xml_url, (xml) => {
    const {width, height, squares, name} = xmlMap(xml);
    const img = {url: img_url, width, height};
    cb({name, squares, img})
  }, (err) => {
    console.log(`GOT ERROR LOADING FILE: ${xml_url} : `, err);
  })

}

// cat *xml | grep -wo 'Special="[^ ]*"'  | sort | uniq | sed 's/.*"\([^"]*\)"/\1/' | sort
// see joueur.js for dice
const xmlMap = (xml) => {
  const parser = new DOMParser();
  const xmlDoc = parser.parseFromString(xml, "text/xml");

  const [header] = xmlDoc.getElementsByTagName("Plateau");
  const width = header.getAttribute("Width");
  const height = header.getAttribute("Height");
  const name = header.getAttribute("NAME");

  const relief2type = {
    "Plat": "flat",
    "Descente": "decrease",
    "Cote": "increase",
    // Droite
  };

  const specials = {
    "Aucun": null,
    "Arrivee": "end",
    "Depart": "start",
    "Sprint": "sprint",
    "Sprint": "sprint",
  }

  const effects = {
    "GlacePilee": "hole",
    "Haie": "hurdle",
    "Surprise": "surprise",
  };

  const xs = Array.from(xmlDoc.getElementsByTagName("Pos"));

  const squares = xs.map(x => {
    // case hors-piste (podium, etc.)
    if (x.getAttribute("S1") == null) return null;

    const id = parseInt(x.getAttribute("ID"));

    const next = [x.getAttribute("S1"), x.getAttribute("S2")].map(x => {
      return x && (id + parseInt(x))
    }).filter(x => x)

    return {
      id, next,
      type: relief2type[x.getAttribute("Relief")],
      special: specials[x.getAttribute("Special")],
      effect: effects[x.getAttribute("Special")],
      // ui
      x: x.getAttribute("X") - 35,
      y: x.getAttribute("Y") - 35,
      width: 70, height: 70,
      no_fill: true,
    }
  }).filter(x => x)

  // change rank: very last start square, should have max_rank

  const starts = squares.filter(x => x.special == "start")
  const real_start = starts.reduce((acc, x) => x.id > acc.id ? x : acc);

  const idx = squares.findIndex(s => s == real_start)

  const ranked_squares = squares.slice(idx + 1).concat(squares.slice(0, idx + 1)).map((s, i) => {
    const special = s.special == "start" && s != real_start ? null : s.special;

    return {...s, rank: i, special};
  })


  return {name, squares: ranked_squares, width, height};
}



// loadMap("FightClub")

export { loadMap }

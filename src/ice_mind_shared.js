const {getShortestPathToSquare, prepareSquare} = require('./ice_rules')


const player_force_select = (game, player_id, reachable_squares) => {
  const players = game.players.map(p => {
    return {...p, selected: player_id == p.id};
  })

  const squares = game.squares.map(s => {
    const is_reachable_square = reachable_squares.indexOf(s.id) != -1;
    return {...s, destination: is_reachable_square ? player_id : false, in_destination: true};
  })

  return {players, squares}
}

const mark_choose_square = (game, player_id, choosable_squares) => {
  return game.squares.map(s => {
    const is_choosable_square = choosable_squares.indexOf(s.id) != -1;
    return {...s, destination: is_choosable_square ? player_id : false, in_destination: true};
  })
}


// can't select in flight
const player_select = (getGame, current_player_id, reachable_squares, render) => {
  return (id) => {
    var game = getGame();

    // can only select current player
    if (id != current_player_id) return;

    const players = game.players.map(p => {
      return id == p.id ? {...p, selected: !p.selected} : {...p, selected: false};
    })

    const p = players.find(p => p.selected)
    var squares = game.squares;

    // same as move_ctrl
    if (p) {
      const s = game.squares.find(s => s.player == id);

      squares = squares.map(s => {
        return reachable_squares.indexOf(s.id) == -1 ? {...s, destination: false, in_destination: true} : {...s, destination: p.id, in_destination: true};
      })
    } else {
      squares = squares.map(s => s.destination ? {...s, destination: false, in_destination: false} : s)
    }

    render({...game, players, squares})
  }
}

// USED BY RESPIRATION ONLY
// TODO: fix by selecting on server side, what squares can be used
const square_select = (getGame, cb) => {
  return (id) => {
    var game = getGame();

    // click makes sens only when a player is selected
    const p = game.players.find(p => p.selected)
    if (!p) return;

    // empty destination
    const square = game.squares.find(s => s.id == id);
    const can_move = square.destination ; // && !s.player FIXME
    if (! can_move) return;

    // add player to square
    const squares = game.squares.map(s => {
      if (id == s.id) {
        return {...s, player: p.id, destination: false, in_destination: false, in_move_path: false}
      }
      if (s.player == p.id) {
        return {...s, player: null, destination: false, in_destination: false, in_move_path: false}
      }
      return {...s, destination: false, in_destination: false, in_move_path: false};
    })

    const players = game.players.map(p2 => {
      return p.id == p2.id ? {...p2, selected: false} : p2
    })

    const start_square = game.squares.find(s => s.inflight == p.id);

    // WARNING: don't use previous squares/players!
    const path_used = getShortestPathToSquare(prepareSquare(game), start_square.id, id)

    cb({players, squares, path_used});
  }
}

const move_to = (getGame, best_path_by_end, cb) => {
  return (id) => {
    const game = getGame();
    const {squares, players, state} = game;
    const { player } = state;

    const {path} = best_path_by_end[id] || {};

    if (path) {
      cb({ players, squares, path_used: path })
    }

  }
}


export { player_select, square_select, player_force_select, move_to, mark_choose_square }



const save = (ered) => {
  localStorage.setItem("game_save", JSON.stringify(ered.getGameSave()))
  console.log("Saved:");
  console.log(print_events());
}

const makeEventPlayers = (game, events) => {

  const event_players = {
    instant: (ered, opts, cb) => {
      events.forEach(e => {
        ered.sendEvent(e);
      })
      cb && cb();
    },
    interval: (ered, opts, cb) => {
      var interval = (opts || {interval: 1000}).interval;

      var i = 0;
      var t = setInterval(() => {
        ered.sendEvent(events[i])

        if (++i >= events.length) {
          clearInterval(t);
          cb && cb()
        }
      }, interval)
    },
    manual: (ered) => {
      // todo
    }
  }

  return { event_players, game, events };
};

const print_events = () => {
  const {game, events} = JSON.parse(localStorage.getItem("game_save"))
  const xs = events.map(ev => "  " + JSON.stringify(ev) + ",").join("\n");
  return "[\n" + xs + "\n]";
}


const load = () => {
  const {game, events} = JSON.parse(localStorage.getItem("game_save"))
  return makeEventPlayers(game, events)
}


const install = () => {
  if (typeof window !== undefined) {
    window.save = save;
    window.load = load;
    window.print_events = print_events;
  }
}

if (typeof window !== undefined) {
  install()
}

export { save, load, install };


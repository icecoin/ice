const {getNextSquares, getAllShortestPath, getPathSquares, getShortestPathToSquare, canSquareAcceptPlayer,
       getPathCost, prepareSquare, isSquareFall, isSquareJumpable, isSquareBlocking} = require('./ice_rules')

const {dijkstra, dijkstra_path} = require('./ice_map');


const {getReducers} = require('./ice_position_reduce')

const group_by = (xs, mapper) => {
  return xs.reduce((acc, x) => {
    const k = mapper(x);
    if (acc[k]) {
      acc[k].push(x)
    } else {
      acc[k] = [x];
    }
    return acc;
  }, {});
}

const gen_range = (len) => {
  var l = [];
  for(var i = 0; i < len; i++) { l.push(i) }
  return l;
}

const dijkstra_best = ({ graph, path }, target_id) => {

  // path can be null
  const [ last_square ] = (path || [{}]).slice(-1);
  if (last_square.id === target_id) {
    return path;
  }

  const score_from_path = (path) => {
    if (path.length == 0) return -1;

    const {rank} = path[path.length - 1];

    const cross = path.slice(0, -1).some((s, idx) => path[idx+1].rank < s.rank)

    // if it crosses add a score so that it is better than any path who doesn't cross
    return cross ? rank + 10000 : rank;
  }

  // get best path by calculating path score for all squares
  const [_score, best_path] = Object.keys(graph).reduce((acc, k) => {
    const [best_score, best_path] = acc;
    const djk_square = graph[k];

    const path = dijkstra_path(djk_square);
    const score = score_from_path(path);

    return score > best_score ? [score, path] : acc;
  }, [-1, []])


  return best_path;
};

/*

turn
start_square: the very square the player started on, useful? -> useful for turn calculation
moves: [{action: , path: }, {action: }]

// breathe, move, fall, attack, defend, aspiration_failure {:followed}, aspiration_success: {:followed}
// path is [{id: ...}, {id: ...}]



*/


/*

TODO:

* map choose
* phacoch display
* display : sprint, attack, dive move, etc.
* path + see player move

Fix: landing square for aspiration (square between last and last - 2)
Enhance ice_mind controller
More even

fix events (from reduce): broadcast, forbid, auto_event
broadcast info


fix: phacoch following + trampoline => DONE best path used now
stupid bug: move then dont move then attack => DONE
0 move squares => DONE
aspiration: Regarder tout le long du chemin utilisé actualisé => OK
ne peut pas attaquer un joueur qui saute => OK
remplace joueur... au sol... => OK (inflight)
BUG: reachable_squares: no move... => OK
BUG: win against phacoch => OK
respiration: regarder si tu te toles => DONE !
carte surprise => DONE!
case : glace pilée, 1 case on se tole => GOOD
case : haie, 2 cases on se tole => GOOD
aspiration => forbid w/ jump square ! => GOOD
force déplacement d'au moins une case ou attaque
  => si déplacement => si pas d'attaque possible, force déplacement => GOOD
  => si attaque possible et pas de déplacement => force attaque => GOOD
attaque sans déplacement : bonus = dé => déplacement non fait puis choix attaque
enhance aspiration (real rules for following) => GOOD
attaque sans déplacement : bonus = dé => GOOD
starting block: D4 for everyone => GOOD
can't attack people in startingblock => GOOD
attack phacoch : bonus?! => GOOD
re-attaque phacoch => GOOD
re-attaque player => GOOD
pas d'échapé si sprint => GOOD
1 crane premier joueur => GOOD
phacochère => GOOD // fix click on phacoch when attacking?!
respiration dans la côte => GOOD
better tour de jeu => GOOD ?
race end => GOOD
increase: D6 - 1 au dé => GOOD (what?! sur l'annonce après le -1 pour l'égalité
echappe: D4 => GOOD
race turn update! => GOOD
map: whose first etc. => GOOD
après premier tour, plus d'annonce => GOOD
dijkstra => GOOD
sprint => GOOD -> reste les joueurs
tour de jeu => BASIC (l'un après l'autre...)
basic position => DONE
add aspiration => DONE
add boomerang => DONE
force select from start => DONE
fix path calculation => DONE

*/

/*


carte surprise
==============
1. autographe (trou): perd un tour => GOOD
2. trampoline : démarre sur un trampoline, D12, ligne droite: tombe sur un joueur avance
3. phacochère : avancer de + ou - 5 cases => GOOD
4. foudre : D6 joueur >= D12 foudre : gagne 3 crâne sinon ko => GOOD
5. 2 dés dans la cote : 1 dé supplémentaire pour tout déplacement dans la côte => GOOD
6. 3 crânes : gagne 3 dés => GOOD


Aspiration
==========
impossible si le joueur à suivre
Entre / démarre dans la cote
Entre dans le sprint
Effectue un sprint
Effectue un combat ou un saut

ou si vous êtes (commence):
- sur le plat et lui dans la descente
- dans la côte et lui sur le plat

transcription


case de départ suiveur/suivi == côte/descente
(peut pas: si joueur qui veut suivre inférieur en possibilité de mouvement à l'autre)

peut faire aspi si:

**le joueur suivi a:**
* mouvement: sans saut et pas de combat
* aspi

* [slope] case départ et case d'arrivée ne sont pas en côté
* [in sprint] case départ => en sprint (?!) => interprétation peu importe, en sprint mais tout seul
* [future sprint] case d'arrivée => futur sprint


Cases départ des deux joueurs :
Type de case égale ou supérieure pour le suiveur

*/


// as soon as player starts: move his state to "inflight", so he can move without hitting himself
const player2flight = (squares, player) => {
  return squares.map(s => {
    return s.player == player ? {...s, inflight: player, player: null} : s;
  })
}

const flight2player = (squares, player) => {
  return squares.map(s => {
    return s.inflight == player ? {...s, inflight: null, player} : s;
  })
}


// move player in flight, to target square
const flight2playerSquare = (squares, player, square) => {
  return squares.map(s => {
    if (s.inflight == player) {
      return {...s, inflight: null, player: square == s.id ? player : s.player};
    }
    if (s.id == square) {
      return {...s, player};
    }
    return s;
  })
}


const getInFlightSquare = (squares, player) => squares.find(s => s.inflight == player);

const changeInFlightPlayerSquare = (squares, player, square) => {
  return squares.map(s => {
    if (square == s.id) {
      return {...s, inflight: player}
    } else if (s.inflight == player) {
      return {...s, inflight: null}
    }
    return s;
  })
}

const changePlayerSquare = (squares, player, square) => {
  return squares.map(s => {
    if (square == s.id) {
      return {...s, player}
    } else if (s.player == player) {
      return {...s, player: null}
    }
    return s;
  })
}

const changePhacochSquare = (squares, square) => {
  return squares.map(s => {
    if (square == s.id) {
      return {...s, phacoch: true}
    } else if (s.phacoch) {
      return {...s, phacoch: null}
    }
    return s;
  })
}

const switchPlayerSquare = (squares, p1, p2, inflight) => {
  const is_p1 = p1 == inflight, is_p2 = p2 == inflight;

  return squares.map(s => {
    if (s.player == p1 || s.inflight == p1) {
      return {...s,
        player: p2 == inflight ? null : p2,
        inflight: p2 == inflight ? p2 : null
      }
    } else if (s.player == p2 || s.inflight == p2) {
      return {...s,
        player: p1 == inflight ? null : p1,
        inflight: p1 == inflight ? p1 : null
      }
    }
    return s;
  })
}


const randomDiceRoll = ({ state }) => {
  // we may not always have a player, can be phacoch
  var {player, nb_dice, dice} = state.params;

  const rollDice = () => (Math.floor(Math.random() * 100) % dice) + 1;
  const dice_rolls = gen_range(nb_dice).map(rollDice)

  return {player, dice_rolls};
}


// eg: add_turn_move(game.turn, player, {action: "breathe", path});
const add_turn_move = (turn, player, move) => {
  const players = turn.players.map(t => {
    return t.player == player ? {...t, moves: t.moves.concat([move])} : t;
  });

  return {...turn, players}
};

// missing first square
const may_update_race_turn = (players, player, full_path) => {
  // IF ENDS ON FALLABLE SQUARE: full_path.length === 1...
  // FIXME tests w/ multiple players
  if (full_path.length <= 1) {
    return players;
  }

  const ranks = full_path.map(s => s.rank);
  // we know its forward when squares are linked through next array
  const forward = full_path[0].next.indexOf(full_path[1].id) !== -1;

  if (forward) {
    // we are moving forward, rank should increase. If it doesn't : we have just made a turn
    const cross_squares = ranks.slice(0, -1).filter((r, i) => ranks[i + 1] < r );
    if (!cross_squares.length) {
      return players;
    }

    return players.map(p => p.id == player ? {...p, race_turn: p.race_turn + cross_squares.length} : p);
  }

  // we are moving backward, rank should decrease. If it doesn't : we have just lost a turn
  const cross_squares = ranks.slice(0, -1).filter((r, i) => ranks[i + 1] > r)
  if (!cross_squares.length) {
    return players;
  }

  return players.map(p => p.id == player ? {...p, race_turn: p.race_turn - cross_squares.length} : p);
}

// Is forward
const may_end_race = (player, full_path, turn) => {
  if (full_path.length <= 1)
    return false

  // player is player, having made the full path (race turn updated)
  const {last_turn} = turn;
  if (player.race_turn < last_turn) return false;

  const last_cross_idx = full_path.slice(0, -1).reduce((acc, s, idx) => {
    return full_path[idx+1].rank < s.rank ? idx : acc;
  }, null)

  // prevent bug: is we crossed the end line and then the start line in one move
  const full_path_after_start = last_cross_idx === null ? full_path : full_path.slice(last_cross_idx)

  return full_path_after_start.slice(0, -1).some((s, i) => {
    return s.special == "end" && full_path_after_start[i + 1].special != "end";
  });

}


// TODO
const attackResolve = (game, { winner, looser, final_max_attacker, inflight }) => {
  const {attacker, defender} = game.state.params;
  const switch_attack = attacker.attack_type == "switch";

  var turn_grounded = 1;
  var status = "ko";

  if (final_max_attacker >= 19) {
    status = "eliminated" ; turn_grounded = null;
  } else if (final_max_attacker >= 16) {
    turn_grounded = 2;
  }


  var players = game.players.map(p => {
    if (p.id == winner) {
      return {...p, skulls: p.skulls + 1};
    } else if (p.id == looser) {
      // attacker.final_max_attacker
      return {...p, status, rise_on_turn: turn_grounded ? game.turn.i + turn_grounded : null};
    }
    return p;
  })

  console.log(`
    Winner is ${winner} and wins 1 skull
    Looser is ${looser} and is ${status == "ko" ? `KO for ${turn_grounded} turns` : " ELIMINATED"}
  `);

  var squares = game.squares;
  var full_attacker_path = [], full_defender_path = []; // add start square

  if (switch_attack && winner == attacker.player) {
    const attacker_square = squares.find(s => s.player == winner || s.inflight == winner);
    const defender_square = squares.find(s => s.player == looser || s.inflight == looser);

    full_attacker_path = [attacker_square, defender_square];
    full_defender_path = full_attacker_path.slice().reverse();

    squares = switchPlayerSquare(squares, winner, looser, inflight);
  }

  if (status == "eliminated") {
    squares = squares.map(s => (s.player == looser || s.inflight == looser) ? {...s, player: null, inflight: null} : s)
  }

  var turn = add_turn_move(game.turn, attacker.player, {action: "attack", path: full_attacker_path.slice(1)});
  turn = add_turn_move(turn, defender.player, {action: "defend", path: full_defender_path.slice(1)});

  players = may_update_race_turn(players, attacker.player, full_attacker_path);
  players = may_update_race_turn(players, defender.player, full_defender_path);

  const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
  return {...game, turn, players, squares, state};
}


// given the squares, last player and nb of squares from it
// return the closest square
const start_square_phacoch = (squares, player, nb) => {
  const square = squares.find(s => s.player == player);

  const {path} = dijkstra(square.id, {
    getSquare: (id) => squares.find(s => s.id == id),
    getNext: (s) => s.prev,
    canTraverse: () => true,
    endTraverse: (s, weight) => weight == nb,
  });

  return path[path.length - 1].id;
}


// highest race_turn, then highest rank, plays first
const make_player_sorter = (squares, players) => {
  const rank_by_player = {};
  const p_squares = game.squares.forEach(s => {
    if (s.player || s.inflight) rank_by_player[s.player || s.inflight] = s.rank;
  })

  return (player_a, player_b) => {
    const {race_turn: race_turn_a} = players.find(p => p.id == player_a);
    const {race_turn: race_turn_b} = players.find(p => p.id == player_b);

    const diff_turn = race_turn_b - race_turn_a;
    return diff_turn !== 0 ? diff_turn : rank_by_player[player_b] - rank_by_player[player_a];
  }
}

const attackableSquares = (game, player) => {
  const {squares, players, turn} = game;
  const player_square = squares.find(s => s.inflight == player);
  const around_squares = player_square.next.concat(player_square.prev);
  // FIXME: better make a test attackable: can't attack KO player
  return (around_squares
    .map(id => squares.find(s => s.id == id))
    .filter(s => {
      if (s.phacoch)
        return true;

      if (!s.player)
        return false;

      const target_player = players.find(p => p.id == s.player);
      const turn_player = turn.players.find(p => p.player == s.player);

      // can't attack a player that is ko, and who didn't move yet
      return target_player.status == "ok" && turn_player.turn != 0;
    })
  );
}

// return aspirable players for player
const aspirablePlayers = (game, player) => {
  const follower_square = getInFlightSquare(game.squares, player);

  const idx = game.turn.players.findIndex(x => x.player == player);

  const prev_players = game.turn.players.slice(0, idx);

  const turn_player = game.turn.players.find(p => p.player == player);
  const players_tried = turn_player.moves.filter(m => m.action == "aspiration_failure").map(m => m.followed)

  // not tried, next
  var aspirable_players = prev_players
    .filter(turn => {
      // keep players that have not been tried
      return players_tried.indexOf(turn.player) === -1;
    })
    .filter(turn => {
    const was_next_to_player = follower_square.next.indexOf(turn.start_square) !== -1;
    return was_next_to_player;
  })


  aspirable_players = aspirable_players
    .filter(tp => {
      const p = game.players.find(p => p.id == tp.player);

      const start_square = game.squares.find(s => s.id == tp.start_square);

      const move_action = tp.moves.find(m => m.action == "move" || m.action == "aspiration_success");
      const end_square = move_action ? move_action.path.slice(-1)[0] : null;

      const {last_turn} = game.turn;

      console.log("move action path: ", move_action);

      const tests = [{
         k: "is ko",
         t: () => p.status == "ko"
        }, {
         k: "did not move or aspi",
         t: () => ! move_action
        }, {
          k: "did attack",
          t: () => tp.moves.some(m => m.action == "attack")
        }, {
          k: "start or finish in slope",
          t: () => end_square && (start_square.type == "increase" || end_square.type == "increase")
        }, {
          k: "in sprint",
          t: () => p.race_turn == last_turn && ["sprint", "end"].some(t => t == start_square.special)
        }, {
          k: "future sprint",
          t: () => end_square && (p.race_turn == last_turn && ["sprint", "end"].some(t => t == end_square.special))
        }, {
          k: "followed starting square is better than follower square",
          t: () => {
            // can't follow if followed square is better than follower square
            return (follower_square.type == "flat" && start_square.type == "decrease")
                || (follower_square.type == "increase" && ["flat", "decrease"].some(t => t == start_square.type))
                || start_square.type == "trampoline";
          }
        // }, {
        //   k: "no empty squares before him",
        //   t: () => end_square && (! end_square.prev.some(id => !game.squares.find(s => s.id == id).player))
        }, {
          k: "did jump",
           // jump if there was a player on a square used in path
          t: () => (move_action ? move_action.path : []).some(isSquareJumpable)
        }, {
         k: "current path blocked or has jump",
         t: () => {
           if (! move_action) {
             return false;
           }

           // very last square may be checked on its side? Then check with slice(0, -2).
           const path_squares = [start_square].concat(move_action.path.slice(0, -1).map(move_s => game.squares.find(s => s.id == move_s.id)));
           const path_squares_prep = prepareSquare({ squares: path_squares, players: game.players })

           // any thing to jump or blocking: can be aspired
           return path_squares_prep.some(s => isSquareJumpable(s) || isSquareBlocking(s))
         }
      }]

      const fails = tests.filter(t => t.t())

      if (fails.length) {
        console.log(`Can't follow player ${tp.player} cause: \n`, fails.map(t => "\t -" + t.k).join('\n'))
      }
      return fails.length == 0;
    })


  const aspirable_player_ids = aspirable_players.map(turn => turn.player);
  console.log("Can follow players:\n" + aspirable_player_ids.map(p => `- ${p}`).join("\n"));

  return aspirable_player_ids;
}

const chooseAspirationSquare = (game, params) => {
  const {follower, followed} = params;
  const s_followed = game.squares.find(s => s.player == followed);
  const choosable_squares = game.squares.filter(s => {
    return s_followed.prev.includes(s.id) && canSquareAcceptPlayer(s)
  }).map(s => s.id);

  return {
    name: "choose_aspiration_square",
    params: {...params, choosable_squares},
    events: ["aspiration_square_chosen"],
    auto: choosable_squares.length == 1 && {square: choosable_squares[0]}
  }
}

const makeStateAttackAction = (game, params) => {
  const attackable_squares = attackableSquares(game, params.player);

  var auto = false;
  if (attackable_squares.length == 1) {
    const s = attackable_squares[0];
    auto = {
      attacker: params.player,
      defender: s.player || s.phacoch
    }
  }

  return {
    name: "attack_action",
    params: {...params, attackable_squares},
    events: ["attack_target_chosen"],
    auto
  }
}


const makeStateChooseAction = (game, params) => {
  const {player} = params;
  const attackable_squares = attackableSquares(game, player);

  const player_square = getInFlightSquare(game.squares, player);
  const reachable_squares = getNextSquares(prepareSquare(game), player_square.id, 1);
  const aspirable_players = aspirablePlayers(game, player);

  const events = [
    reachable_squares.length && "choose_breathe_action",
    reachable_squares.length && "choose_move_pick_skull",
    aspirable_players.length && "choose_aspiration",
    attackable_squares.length && "choose_attack"
  ].filter(x => x);

  return {name: "choose_action", params, events}
}


const getBestNextSquares = ({rank, next}, squares) => {
  const next_squares = squares.filter(s => next.indexOf(s.id) !== -1);
  const next_turn_squares = next_squares.filter(s => s.rank < rank)

  // work on squares that are on next turn if it exists
  const sel_squares = next_turn_squares.length ? next_turn_squares : next_squares;

  // sort highest rank
  return sel_squares.sort((a, b) => b.rank - a.rank)
}


const trampoline_player_fall_ahead = (squares, landing_square) => {
  var moves = [], current_square = landing_square;

  // look if there is a player on square,
  // if so, decide on which square he will be moved
  // then loop to see if this square has also a player etc.
  while (true) {
    if (! current_square.player) break;

    // could be changed so that we don't choose the best next square, but also the empty one
    const [ move_to_square ] = getBestNextSquares(current_square, squares);
    moves.push({from: current_square, to: move_to_square});

    current_square = move_to_square;
  }

  // sorted from nearest to farthest to landing square
  return moves;
}

// get square where to fall
const fall_behind_path = (full_path) => {
  const full_path_rev = full_path.slice().reverse();
  const empty_square_idx = full_path_rev.findIndex(s => !s.player)
  // includes last square
  return full_path_rev.slice(0, empty_square_idx + 1);
}


const fall_behind = ({ players, squares, turn }, player, full_path) => {
  players = players.map(p => {
    return p.id == player ? {...p, status: "ko", rise_on_turn: game.turn.i + 1} : p;
  })

  // did not move, can't fall behind, fall on himself
  if (full_path.length <= 1) {
    turn = add_turn_move(turn, player, {action: "fall", path: full_path});
  } else {
    const full_fall_path = fall_behind_path(full_path);
    const empty_square = full_fall_path[full_fall_path.length - 1];

    turn = add_turn_move(turn, player, {action: "fall", path: full_fall_path.slice(1)});
    players = may_update_race_turn(players, player, full_fall_path);

    squares = changeInFlightPlayerSquare(squares, player, empty_square.id);
  }

  return { players, squares, turn };
}


// reducers could indicate what they accept...
// they won't be only reducers but state too?

const reducers = {
  init: (game, e) => {
    const {players} = e.params

    // players sorted: from first to last
    // create turn struct

    const [first_player] = players;

    var place_square = game.squares.find(s => s.special == "start");

    const players_turn = players.map(player_id => {
      const p = {player: player_id, turn: 0, start_square: place_square.id, moves: []};
      place_square = game.squares.find(s => s.rank == place_square.rank - 1);
      return p;
    })

    const last_turn = 1;
    const turn = {i: 0, last_turn, players: players_turn};
    // set race_turn to -1, add one skull to 1st in position
    const game_players = game.players.map(p => {
      return {...p, race_turn: -1, status: "ok", items: [], skulls: p.id == first_player ? p.skulls + 1 : p.skulls }
    })

    // from player score to initial square
    var squares = game.squares.map(s => {
      const t = players_turn.find(t => s.id == t.start_square);
      return t ? {...s, player: t.player} : s;
    })

    const phacoch = {...game.phacoch, race_turn: -1,  status: "ok", move: 5};
    // need squares w/ players on it
    const phacoch_nb_away = 5;
    const phacoch_square = start_square_phacoch(squares, players[players.length -1], phacoch_nb_away);

    squares = squares.map(s => s.id == phacoch_square ? {...s, phacoch: true} : s);

    const state = {name: "state_turn_start", events: ["state_turn_start"], auto: true};

    return {...game, turn, state, squares, players: game_players, phacoch};
  },
  state_race_end: (game, e) => {
    const nth_finish_players = game.players.filter(p => p.status == "finish").sort((a, b) => a.finish_nth - b.finish_nth)
    const eliminated = game.players.filter(p => p.status == "eliminated")

    console.log("\n\n### RACE ENDED ###\n\n");

    console.log("Players ranking: ");
    console.log(nth_finish_players.map((p, i) => `  #${i+1}: ${p.id}`).join("\n"))

    console.log("\nElimination: ");
    console.log(eliminated.map(p => `  ${p.id}`).join("\n"))

    return {...game, state: {name: "end_race_action"}};
  },
  state_turn_start: (game, e) => {
    console.log("NEW TURN");

    const did_sprint_players = (e.params || {}).sprint_players || [];

    // TODO:
    // * set moves to empty array
    // * reinit his start_square (take last player path if any, or look up squares)

    const {last_turn} = game.turn;
    const sprintable_players = game.players.filter(p => p.status == "ok" && p.race_turn == last_turn).filter(p => {
      const square = game.squares.find(s => s.player == p.id);
      return ["sprint", "end"].indexOf(square.special) !== -1
    }).map(p => p.id);


    var turn = game.turn;
    var turn_players = turn.players;

    // first pass
    if (!did_sprint_players.length || sprintable_players.length == 0) {
      // sprint info, squares, moves
      turn_players = turn.players.map(p => {
        const in_sprint = sprintable_players.length > 1 && sprintable_players.indexOf(p.player) !== -1

        const s = game.squares.find(s => s.player == p.player);
        return {...p, in_sprint, moves: [], start_square: s.id};
      })

      turn = {...turn, i: game.turn.i + 1, players: turn_players};
    }


    // we have a sprint todo
    if (!did_sprint_players.length && sprintable_players.length > 1) {
      // first time we enter this turn, inc turn

      console.log("In sprint", sprintable_players)
      const state = {
        name: "state_sprint_start",
        events: ["sprint_start"],
        params: { players: sprintable_players },
        auto: true
      };

      return {...game, turn, state};
    }

    const player_sorter = make_player_sorter(game.squares, game.players);

    const not_in_sprint_players = turn_players.filter(p => did_sprint_players.indexOf(p.player) === -1);

    const ordered_not_sprint_turn_players = not_in_sprint_players.slice().sort((a, b) => {
      return player_sorter(a.player, b.player);
    })

    const ordered_sprint_turn_players = did_sprint_players.map(id => turn.players.find(p => p.player == id));

    turn_players = ordered_sprint_turn_players.concat(ordered_not_sprint_turn_players);

    const state= {name: "loop_turn", events: ["loop_turn"], auto: true};
    return {...game, turn: {...turn, players: turn_players}, state};
  },
  state_turn_end: (game, e) => {
    const {turn} = game;

    // removed done players for next turn (they have already been cleared from squares)
    const eliminated_players = game.players.filter(p => p.status == "eliminated").map(p => p.id);
    const finish_players = game.players.filter(p => p.status == "finish").map(p => p.id);
    const done_players = eliminated_players.concat(finish_players);

    const turn_players = turn.players.filter(p => done_players.indexOf(p.player) === -1);

    if (turn_players.length == 0) {
      // As turn end, no more players to play: game ends
      const state = {name: "state_race_end", events: ["state_race_end"], auto: true};
      return {...game, state, turn: null};
    } else {
      // New turn

      // rise players
      const players = game.players.map(p => {
        return p.rise_on_turn == turn.i ? {...p, status: "ok", rise_on_turn: null} : p;
      })

      // rise phacoch
      var ph = game.phacoch;
      const phacoch = ph.rise_on_turn == turn.i ? {...ph, status: "ok", rise_on_turn: null} : ph;

      const state = {name: "state_turn_start", events: ["state_turn_start"], auto: true};
      return {...game, state, turn: {...turn, players: turn_players}, players, phacoch};
    }
  },
  state_turn_player_next: (game, e) => {
    const {players, turn: gturn} = game;

    const {player} = gturn.players.find(p => p.turn < gturn.i);
    console.log(`NEW PLAYER TURN ${player}`);

    const tp = gturn.players.map(p => p.player == player ? {...p, current: true} : p);
    const turn = {...gturn, players: tp};

    const squares = player2flight(game.squares, player);

    // if KO or eliminated: this guy does not play, end his turn immediately
    if (players.find(p => p.id == player).status != "ok") {
      const state = {name: "state_turn_player_end", events: ["state_turn_player_end"], auto: true};
      return {...game, state, turn, squares};
    } else {
      // should do a state: state_turn_player_start

      const new_game = {...game, turn, squares};

      const state = makeStateChooseAction(new_game, {player});

      return {...new_game, state};
    }
  },
  state_turn_player_end: (game, e) => {
    const {i, players: t_players} = game.turn;

    const {player} = t_players.find(p => p.turn < i);
    console.log(`PLAYER TURN END ${player}`);

    const new_players_turn = t_players.map(tp => tp.player == player ? {...tp, turn: tp.turn + 1, current: false} : tp)
    var turn = {...game.turn, players: new_players_turn}


    const p = game.players.find(p => p.id == player)

    var players = game.players;
    var squares = game.squares;

    // status : eliminated is done elsewhere, because you can be eliminated and it's not your turn
    if (p.status == "finish") {
      // if finish: don't flight2player, remove it for good
      squares = squares.map(s => s.inflight == player ? {...s, inflight: null} : s)

      // player is himself part of the players
      const finish_nth = game.players.filter(p => p.status == "finish").length - 1;

      players = players.map(p => p.id == player ? {...p, finish_nth} : p)
    } else {

      // if ok, test, fall
      if (p.status == "ok") {
        const last_square = getInFlightSquare(squares, player);
        const [prep_last_square] = prepareSquare({ squares: [ last_square ], players })

        if (isSquareFall(prep_last_square)) {
          const tp = turn.players.find(p => p.player == player)

          const start_square = squares.find(s => s.id == tp.start_square);
          // we use this because we know we will find an empty square at the beggining of our turn: ours
          const full_path = [].concat.apply([start_square], tp.moves.map(x => x.path || []));
          // debugger;

          // moves
          var {players, squares, turn} = fall_behind({players, squares, turn}, player, full_path);
        }
      }

      squares = flight2player(squares, player);
    }

    const state = {name: "loop_turn", events: ["loop_turn"], auto: true};
    return {...game, state, turn, squares, players};
  },
  loop_turn: (game, e) => {
    const {turn} = game;
    const cont = turn.players.some(p => p.turn < turn.i);

    if (cont) {
      // if ko or eliminated, the turn will end directly
      const state = {name: "state_turn_player_next", events: ["state_turn_player_next"], auto: true};
      return {...game, state};
    } else {
      const has_player = game.players.some(({status}) => status != "eliminated" && status != "finish")

      if (has_player) {
        // TURN END => PHACOCH TURN
        const state = {name: "state_turn_phacoch_start", events: ["state_turn_phacoch_start"], auto: true};
        return {...game, state};
      } else {
        // Last players is eliminated or finished? no one to follow for phacoch, end turn
        const state = {name: "state_turn_end", events: ["state_turn_end"], auto: true};
        return {...game, state};
      }
    }
  },
  state_turn_player_start: (game, e) => {
  },
  choose_breathe_action: (game, e) => {
    const {player} = e.params
    // TODO STATE 2: precalculate squares

    const player_square = getInFlightSquare(game.squares, player);
    const reachable_squares = getNextSquares(prepareSquare(game), player_square.id, 1);

    const state = {
      name: "breathe_action", params: {player, reachable_squares},
      events: ["breathe_result"], auto: reachable_squares.length == 1 && {player, square: reachable_squares[0]}
    }
    return {...game, state}
  },
  choose_attack: (game, e) => {
    const {player, score} = game.state.params
    // play same as movement (but attack only)
    const state = {name: "state_attack_action", params: { player }, events: ["choose_move_pick_skull"], auto: { player, attack_only: true } }
    return {...game, state};
  },
  choose_attack_after_move: (game, e) => {
    const {player, score} = game.state.params

    const state = makeStateAttackAction(game, {player, score})

    return {...game, state};
  },
  breathe_result: (game, e) => {
    const {player, square} = e.params

    const start_square = game.squares.find(s => s.inflight  == player);
    const dont_move = start_square.type == "increase"; // we have minus one on it...

    const full_path = dont_move ? [start_square] : [start_square, game.squares.find(s => s.id == square)];

    var squares = dont_move ? game.squares : changeInFlightPlayerSquare(game.squares, player, square);

    var players = game.players.map(p => {
      return p.id == player ? {...p, skulls: p.skulls + 1} : p;
    })

    var turn = add_turn_move(game.turn, player, {action: "breathe", path: full_path.slice(1)});
    players = may_update_race_turn(players, player, full_path);

    const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};

    if (may_end_race(players.find(p => p.id == player), full_path, turn)) {
      players = players.map(p => p.id == player ? {...p, status: "finish"} : p)

      return {...game, turn, squares, players, state};
    }

    return {...game, turn, squares, players, state}
  },
  choose_move_pick_skull: (game, e) => {
    const {player, attack_only} = e.params
    const state = {
      name: "pick_skull_action", params: {player, attack_only},
      events: ["move_picked_skull"],
      auto: !game.players.find(p => p.id == player).skulls && {player, skulls: 0}
    };
    return {...game, state}
  },
  move_picked_skull: (game, e) => {
    const {player, skulls} = e.params
    const { attack_only } = game.state.params;

    const players = game.players.map(p => {
      return p.id == player ? {...p, skulls: p.skulls - skulls} : p;
    })

    // normal dice (can be overriden)
    const square = getInFlightSquare(game.squares, player);
    var dice = {flat: 6, increase: 6, decrease: 8, trampoline: 12}[square.type];

    const [{player: first_player_id, in_sprint}] = game.turn.players;

    if (first_player_id == player) {
      const has_neighbour = square.next.concat(square.prev).some(id => {
        return game.squares.find(s => s.id == id).player;
      })
      console.log("Échappée ?", !has_neighbour && !in_sprint);

      if (!has_neighbour && !in_sprint && square.type != "decrease" && square.type != "trampoline") {
        // on sprint, decrease, trampoline: use type of square
        dice = 4;
      }
    }

    const turn_player = game.turn.players.find(p => p.player == player);
    // starting block;
    if (turn_player.turn == 0) {
      dice = 4;
    }

    const {race_turn} = players.find(p => p.id == player);

    const has_extra_slope_dice = players.find(p => p.id == player).items.find(({k}) => k == "slope_extra_dice")
    var extra_dice = (square.type == "increase" && has_extra_slope_dice) ? 1 : 0;

    console.log("EXTRA DICE: ", extra_dice);

    const nb_dice = 1 + skulls + extra_dice;

    var state;
    // we are on last turn or attack only (no use for announce): no more announce
    console.log("race_turn == game.turn.last_turn", race_turn, game.turn.last_turn);
    console.log("attack_only", attack_only);

    if (race_turn == game.turn.last_turn || attack_only) {
      state = {name: "dice_roll", params: {player, nb_dice, dice, attack_only}, events: ["move_dice_roll_done"]}
    } else {
      state = {name: "announce_action", params: {player, nb_dice, dice}, events: ["announce_action_done"]};
    }

    return {...game, players, state}
  },
  announce_action_done: (game, e) => {
    const {player, nb_dice, dice} = game.state.params
    const {announce} = e.params

    const state = {name: "dice_roll", params: {player, nb_dice, dice, announce}, events: ["move_dice_roll_done"]}
    return {...game, state}
  },
  announce_boomerang_done: (game, e) => {
    const {player, defender, attacker} = game.state.params
    const {announce} = e.params

    const state = {
      name: "dice_roll",
      params: {
        dice: 12, nb_dice: 1,
        player, announce, defender, attacker
      },
      events: ["boomerang_dice_rolled"]
    }
    return {...game, state}
  },
  move_dice_roll_done: (game, e) => {
    const {announce, attack_only} = game.state.params;
    const {player, dice_rolls} = e.params;

    const square = getInFlightSquare(game.squares, player);
    const minus_one = square.type == "increase";

    const getScore = (dice_rolls, announce, attack_only) => {
      var [max] = dice_rolls.sort((a, b) => b - a)
      max = minus_one ? max - 1 : max;

      // no more announce => we must be in last turn
      if (announce === false || attack_only) {
        return max
      }

      if (dice_rolls.some(roll => (minus_one ? roll - 1 : roll) >= announce)) {
        const bonus = dice_rolls.filter(roll => roll == announce).length;
        return announce + max + bonus;
      } else {
        return max;
      }
    }
    const score = getScore(dice_rolls, announce, false);

    if (square.type == "trampoline") {
      const state = {name: "trampoline_move", events: ["trampoline_move"], auto: {player, score}};
      return {...game, state};
    }

    const score_no_move = getScore(dice_rolls, announce, true);
    const attackable_squares = attackableSquares(game, player);

    if (attack_only) {
      const state = makeStateAttackAction(game, {player, attack_only, score: score_no_move})

      return {...game, state};
    } else {
      // can end move directly witouth even moving if you can attack

      // if score is 0: only enables move_end type and make it auto
      // if attackable_squares: you may end your move directy but you will be forced to attack after
      const events = [
        (score == 0 || attackable_squares.length) && "move_end",
        score != 0 && "move_made",
      ].filter(x => x)
      const auto = score == 0;

      const state = {name: "moving", params: {player, score, score_no_move, prev_paths: []}, events, auto}
      return {...game, state};
    }
  },
  move_made: (game, e) => {
    const {path_used, is_trampoline} = e.params
    const {player, score, prev_paths} = game.state.params

    const start_square = getInFlightSquare(game.squares, player);
    const square_used = path_used.map(id => game.squares.find(s => s.id == id))
    const path_square_used = prepareSquare({ players: game.players, squares: square_used });

    var path_cost;

    if (start_square.type == "trampoline") {
      path_cost = path_used.length;
    } else {
      // respect: order can't do (game.squares.filter(s => path_used.indexOf(s.id) !== -1))
      path_cost = getPathCost(path_square_used);
    }

    const new_score = score - path_cost;
    const new_prev_paths = prev_paths.concat([path_used]);

    // always update players race turn when move finishes (do not wait for end of move, to be precise)
    const full_square_used = [start_square].concat(square_used)

    var players = may_update_race_turn(game.players, player, full_square_used);

    if (may_end_race(players.find(p => p.id == player), full_square_used, game.turn)) {
      players = players.map(p => p.id == player ? {...p, status: "finish"} : p)

      const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
      return {...game, players, state}
    }

    const square = path_used[path_used.length - 1];
    const squares = changeInFlightPlayerSquare(game.squares, player, square);

    if (new_score > 0) {
      // may move again
      const events = ["move_made", "move_end"];
      const state = {name: "moving", params: {player, score: new_score, prev_paths: new_prev_paths}, events};
      return {...game, squares, players, state}
    }

    // trampoline will be there
    if (new_score == 0) {
      // move end
      const state = {
        name: "state_move_end", params: {player, score: new_score, prev_paths: new_prev_paths},
        events: ["move_end"], auto: true
      };
      return {...game, squares, players, state};
    }

    const last_square_prep = path_square_used[path_square_used.length - 1];
    if (new_score == -1 && last_square_prep.cost > 1 && isSquareJumpable(last_square_prep)) {
      // we end on a player/hurdle but can not pass it...  we will fall: move end
      const state = {
        name: "state_move_end", params: {player, score: new_score, prev_paths: new_prev_paths},
        events: ["move_end"], auto: true
      };
      return {...game, squares, players, state}
    }

    throw `Should not happen negative score after move! ${new_score}`;
  },
  move_end: (game, e) => {
    const {player, score, score_no_move, prev_paths, fall} = game.state.params

    // aggregate all path in one: with no players!
    const path = [].concat.apply([], prev_paths)
      .map(id => {
        const s = game.squares.find(s => s.id == id);
        // fix glitch: player should not be on his own square, but he is
        // FIXME: May not be anymore since the "inflight" change
        return s.player == player ? {...s, player: null} : s;
      });

    const tp = game.turn.players.find(tp => tp.player == player);
    // get start square as last square in moves or start_square: very start square at start of flight
    // const start_square = tp.moves.slice().reverse().find(m => m.path && m.path[0]) || tp.start_square;
    const start_square = game.squares.find(s => s.id == tp.start_square);


    const did_move = path.length != 0;
    const last_square = did_move ? path[path.length - 1] : start_square;

    const attackable_squares = attackableSquares(game, player);

    // no check for may_update_race_turn: already did along move
    var turn = game.turn;

    if (did_move) {
      turn = add_turn_move(game.turn, player, {action: "move", path})
    }


    var players = game.players;
    var squares = game.squares;

    if (start_square.type == "trampoline") {

      // remove player from square (in flight!)
      // squares = squares.map(s => s.player == player ? {...s, player: null} : s);

      const landing_square = path[path.length - 1];
      // move every player one square ahead
      const fall_aheads = trampoline_player_fall_ahead(squares, landing_square);

      // put all players ko
      const fall_players = fall_aheads.map(({from}) => from.player)
      players = players.map(p => {
        return fall_players.indexOf(p.id) !== -1 ? {...p, status: "ko", rise_on_turn: game.turn.i + 1} : p;
      })

      console.log("FALL PLAYER", JSON.stringify(fall_aheads, null, 2));

      // move them, from last to first
      fall_aheads.slice().reverse().map(({from, to}) => {
        const {player: fall_player} = from;

        squares = changePlayerSquare(squares, fall_player, to.id);
        turn = add_turn_move(turn, fall_player, {action: "fall_ahead", path: [to]});
        players = may_update_race_turn(players, fall_player, [from, to]);

        if (may_end_race(players.find(p => p.id == fall_player), [from, to], turn)) {
          players = players.map(p => p.id == fall_player ? {...p, status: "finish"} : p)
        }
      })

      // lands
      // squares = squares.map(s => s.id == landing_square.id ? {...s, player} : s);
    }


    const [prep_last_square] = prepareSquare({ squares: [ last_square ], players })

    const cant_attack = isSquareJumpable(prep_last_square) || (did_move && start_square.type == "trampoline");
    if (cant_attack) {
      const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true}
      return {...game, turn, players, squares, state};
    }

    // if score was 0 from the start, he didn't choose not to move, so he can choose to not attack
    if (did_move || score == 0) {

      // if can't attack... then auto pass attack
      if (attackable_squares.length == 0) {
        const state = {name: "may_attack_action", events: ["pass_attack"], auto: {player}};
        return {...game, turn, squares, state};
      }

      const events = ["choose_attack_after_move", "pass_attack"];
      const state = {name: "may_attack_action", params: {player, score, attackable_squares}, events};
      return {...game, turn, squares, state}
    } else {
      // force attack!
      const state = makeStateAttackAction(game, {player, attack_only: true, score: score_no_move});

      return {...game, squares, state};
    }
  },
  pass_attack: (game, e) => {
    const {player} = e.params;

    const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
    return {...game, state};
  },
  attack_target_chosen: (game, e) => {
    const {score} = game.state.params;
    const {attacker, defender} = e.params;

    const attacker_square = getInFlightSquare(game.squares, attacker);

    if (defender === true) {
      // defender is phacocher
      const state = {
        name: "state_phacoch_attack_action", params: {end_phacoch_event: "state_turn_player_may_end", phacoch_defending: true},
        events: ["state_phacoch_attack_action"], auto: {player: attacker, score}
      };

      return {...game, state};
    }

    const defender_square = game.squares.find(s => s.player == defender);
    const is_defender_ahead = attacker_square.next.some(id => id == defender_square.id)

    // skip choose attack tyoe if defender is not ahead
    const state = {
      name: "choose_attack_type",
      params: {
        player: attacker,
        attacker: {player: attacker, score},
        defender: {player: defender}
      },
      events: ["attack_type_chosen"],
      auto: !is_defender_ahead && {attack_type: "normal", player: attacker}
    };

    return {...game, state};
  },
  attack_type_chosen: (game, e) => {
    const {player, attacker, defender} = game.state.params;
    const {attack_type} = e.params;

    const state = {
      name: "pick_skull_action",
      params: {
        player,
        attacker: {...attacker, attack_type},
        defender
      },
      events: ["picked_skull_for_attack"],
      auto: !game.players.find(p => p.id == player).skulls && {player, skulls: 0}
    };
    return {...game, state};
  },
  picked_skull_for_attack: (game, e) => {
    const {player, skulls} = e.params;
    const {defender, attacker} = game.state.params;

    const players = game.players.map(p => {
      return p.id == player ? {...p, skulls: p.skulls - skulls} : p;
    })

    const nb_dice = 1 + skulls;

    var state;
    if (player == attacker.player) {
      state = {
        name: "pick_skull_action",
        params: {
          player: defender.player,
          attacker: {...attacker, nb_dice},
          defender
        },
        events: ["picked_skull_for_attack"],
        auto: !game.players.find(p => p.id == defender.player).skulls && {player: defender.player, skulls: 0}
      }
    } else {
      state = {
        name: "dice_roll",
        params: {
          player: attacker.player,
          nb_dice: attacker.nb_dice, dice: 12,

          attacker,
          defender: {...defender, nb_dice}
        },
        events: ["attack_dice_rolled"],
      }
    }

    return {...game, players, state}
  },
  attack_dice_rolled: (game, e) => {
    const {player, dice_rolls} = e.params;
    var {defender, attacker} = game.state.params;


    // either dice_rolls is already set or we are setting it at this function call
    const [max_attacker] = (attacker.dice_rolls || dice_rolls).sort((a, b) => b - a);
    const switch_attack = attacker.attack_type == "switch";
    const modifier = switch_attack ? -1 : 0;

    const bonus_max_attacker = max_attacker + attacker.score;
    const final_max_attacker = max_attacker + attacker.score + modifier ;

    // save the final max attacker for future use (boomerang, to calculate the hit)
    attacker = player == attacker.player ? {...attacker, dice_rolls, final_max_attacker} : attacker;

    // defend time for p2
    if (player == attacker.player && final_max_attacker <= 12) {
      const state = {
        name: "dice_roll",
        params: {
          player: defender.player,
          nb_dice: defender.nb_dice, dice: 12,
          attacker, defender
        },
        events: ["attack_dice_rolled"]
      }
      return {...game, state}
    } else if (player == attacker.player && final_max_attacker > 12) {
      // boomerang mode
      const state = {
        name: "announce_action",
        params: {
          player: defender.player,
          attacker, defender
        },
        events: ["announce_boomerang_done"]
      }
      return {...game, state}
    }

    // resolve the fight!

    const [max_defender] = dice_rolls.sort((a, b) => b - a);

    console.log(`
      attacker: ${attacker.dice_rolls.join(" ")}
      defender: ${dice_rolls.join(" ")}

      max_attacker: ${max_attacker}
      max_attacker with bonus : ${final_max_attacker} (+ ${attacker.score} bonus + ${modifier} )
      max_defender: ${max_defender}
    `);


    if (final_max_attacker == max_defender) {
      console.log(`Equality : ${attacker.player} and ${defender.player} stays the same`);

      // TODO STATE 2: WARNING about phacoch defending, did we erase the state?!!
      const events = ["phacoch_reattack_continue", "reattack_continue", "phacoch_reattack_cancel", "reattack_cancel"];
      return {...game, state: {name: "may_reattack_action", params: {defender, attacker}, events}};
    }

    const [winner, looser] = (final_max_attacker > max_defender ? [attacker, defender] : [defender, attacker]).map(x => x.player);

    // get current player
    const {player: inflight_player} = game.turn.players.find(p => p.turn < game.turn.i);

    return attackResolve(game, { winner, looser, final_max_attacker: attacker.final_max_attacker, inflight: inflight_player });
  },
  boomerang_dice_rolled: (game, e) => {
    const {player, announce, defender, attacker, final_max_attacker} = game.state.params;
    const {player: inflight_player} = game.turn.players.find(p => p.turn < game.turn.i);

    const [dice_roll] = e.params.dice_rolls;

    const boomerang_success = dice_roll == announce;

    console.log(`boomerang de ${player} : ${boomerang_success ? "Succès" : "Échec"} ! `);

    const [winner, looser] = (boomerang_success ?  [defender, attacker] : [attacker, defender]).map(x => x.player);

    return attackResolve(game, {winner, looser, final_max_attacker: attacker.final_max_attacker, inflight: inflight_player })
  },


  choose_aspiration: (game, e) => {
    const {player} = game.state.params;

    const aspirable_players = aspirablePlayers(game, player);
    const aspirable_squares = game.squares.filter(s => aspirable_players.includes(s.player));

    // auto if only one player to aspire
    const state = {
      name: "choose_aspiration_player", params: { player, aspirable_squares },
      events: ["aspiration_target_chosen"], auto: aspirable_players.length == 1 && {followed: aspirable_players[0]}
    }
    return {...game, state}
  },

  aspiration_target_chosen: (game, e) => {
    const {player} = game.state.params;
    const {followed} = e.params;

    const state = {
      name: "ask_accept_aspiration",
      params: { player: followed, followed, follower: player },
      events: ["aspiration_allow", "aspiration_deny"]
    }
    return {...game, state}
  },

  aspiration_allow: (game, e) => {
    const {follower, followed} = game.state.params;

    const state = chooseAspirationSquare(game, { player: follower, follower, followed })

    return {...game, state}
  },


  aspiration_deny: (game, e) => {
    const {follower, followed} = game.state.params;

    // aspiration_choose_square
    const state = {
      name: "aspiration_challenge_followed",
      params: { player: followed, follower, followed},
      events: ["aspiration_challenge_followed_done"]
    }
    return {...game, state}
  },

  aspiration_challenge_followed_done: (game, e) => {
    const {card} = e.params;
    const {follower, followed} = game.state.params;

    // aspiration_choose_square
    const state = {
      name: "aspiration_challenge_follower",
      params: { player: follower, followed, follower, followed_card: card},
      events: ["aspiration_challenge_follower_done"]
    }
    return {...game, state}
  },

  aspiration_challenge_follower_done: (game, e) => {
    const {card} = e.params;
    const {follower, followed, followed_card} = game.state.params;

    const aspiration_success = card == followed_card;

    if (aspiration_success) {
      const state = chooseAspirationSquare(game, { player: follower, follower, followed})
      return {...game, state}
    } else {
      // reset turn of current player
      const turn = add_turn_move(game.turn, follower, {action: "aspiration_failure", followed});

      const new_game = {...game, turn}

      const state = makeStateChooseAction(new_game, { player: follower });

      return {...new_game, state};
    }
  },


  aspiration_square_chosen: (game, e) => {
    const {follower, followed} = game.state.params;
    const { square } = e.params;

    const squares = changeInFlightPlayerSquare(game.squares, follower, square);

    const s_follower = getInFlightSquare(game.squares, follower);
    const path = getShortestPathToSquare(prepareSquare(game), s_follower.id, square);

    const aspi_path = path.map(square_path => game.squares.find(s => s.id == square_path.id));
    const turn = add_turn_move(game.turn, follower, {action: "aspiration_success", path: aspi_path, followed});

    const players = may_update_race_turn(game.players, follower, [s_follower].concat(aspi_path));

    const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
    return {...game, turn, players, squares, state};
  },

  reattack_cancel: (game, e) => {
    const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
    return {...game, state};
  },
  reattack_continue: (game, e) => {
    const {defender, attacker} = game.state.params;

    const state = {
      name: "state_attack_target_chosen", params: {score: 0},
      events: ["attack_target_chosen"],
      auto: {
        attacker: attacker.player,
        defender: defender.player
      }
    }
    return {...game, state};
  },

  // player was attacking phacoch, and may reattack, fool!
  phacoch_reattack_cancel: (game, e) => {
    const state = {name: "state_turn_player_may_end", events: ["state_turn_player_may_end"], auto: true};
    return {...game, state};
  },
  phacoch_reattack_continue: (game, e) => {
    const {defender, attacker} = game.state.params;

    const state = {
      name: "state_attack_target_chosen", params: {score: 0},
      events: ["attack_target_chosen"],
      auto: {
        attacker: defender.player,
        defender: true
      }
    }
    return {...game, state};
  },

  //
  state_turn_player_may_end: (game, e) => {
    const {i, players: t_players} = game.turn;
    const tplayer = t_players.find(p => p.turn < i);
    const player = game.players.find(p => p.id == tplayer.player);

    const state_player_end = {name: "state_turn_player_end", events: ["state_turn_player_end"], auto: true};

    if (player.status != "ok") {
      return {...game, state: state_player_end};
    }

    const last_square = getInFlightSquare(game.squares, player.id);

    if (isSquareFall(last_square)) {
      return {...game, state: state_player_end};
    }

    if (last_square.effect == "surprise") {
      const state = {
        name: "state_surprise_start", params: {player: player.id},
        events: ["state_surprise_start"], auto: true
      };
      return {...game, state};
    }

    return {...game, state: state_player_end};
  },

  trampoline_move: (game, e) => {
    // force path and full path

    const {player, score} = e.params;
    const square = getInFlightSquare(game.squares, player);


    const start = game.squares.find(s => s.special == "start");

    var current_square = square.id,
        current_score = score;

    var path = [];

    // ... if you make several turn of board
    while (true) {
      const res = dijkstra(current_square, {
        getSquare: (id) => game.squares.find(s => s.id == id),
        getNext: (s) => s.next,
        canTraverse: (s, weight) => weight < current_score,
        endTraverse: (s, weight) => s.special == "start" && weight != 0
      });

      const current_path = dijkstra_best(res, false);

      path = path.concat(current_path)

      const {id: last_id} = current_path[current_path.length - 1];
      const w = res.graph[last_id].weight;

      // we reached what we wanted
      if (w == current_score) {
        break;
      } else {
        current_square = last_id;
        current_score = current_score - w;
      }
    }

    const state = {
      name: "move_made", params: {player, score, prev_paths: []},
      events: ["move_made"],
      auto: { path_used: path.map(p => p.id), is_trampoline: true }
    };

    return {...game, state};
  }

};


const sprint_reducers = {
  sprint_start: (game, e) => {
    const {players} = game.state.params;

    const players_group = [
      players.map(id => ({id, split_dice_roll: null}))
    ];

    // present stuff as if it was already a split
    const state = {
      name: "state_loop_sprint", params: { players: [], players_group },
      events: ["sprint_split_loop"], auto: true
    };

    return {...game, state};
  },
  sprint_end: (game, e) => {
    const {players} = game.state.params;

    // players is sorted ascending, make it DESC so that first player plays first
    const ordered_sprint_players = players.slice().reverse();
    const state = {
      state: "state_turn_start",
      events: ["state_turn_start"], auto: {sprint_players: ordered_sprint_players}
    };

    return {...game, state};
  },

  // same as position_init_split_next
  sprint_split_loop: (game, e) => {
    const { players_group, players } = game.state.params;

    // players_group is sorted ascending (from lowest to highest)
    var idx = players_group.findIndex(pg => pg.length > 1);
    idx = idx === -1 ? players_group.length + 1 : idx;

    const done = players_group.slice(0, idx).map(([p]) => p.id);
    const pending_pgroups = players_group.slice(idx);

    const done_players = players.concat(done);
    const [pgroup, ...rest_pgroups] = pending_pgroups;

    if (! pgroup) {
      // done_players is sorted ascending
      const state = {
        name: "state_sprint_end", params: {players: done_players},
        events: ["sprint_end"], auto: true
      };
      return {...game, state};
    } else {
      const state = {
        name: "state_sprint_resolve_split",
        params: {players: done_players, players_group: rest_pgroups, pgroup},
        events: ["sprint_resolve_split"], auto: true
      };
      return {...game, state};
    }
  },

  // same as: position_split_next
  sprint_resolve_split: (game, e) => {
    const { pgroup, players, players_group } = game.state.params;
    const p = pgroup.find(p => ! p.split_dice_roll);

    // every player in group has roll, we can sort ASC
    if (!p) {
      const p_by_dice = group_by(pgroup, x => x.split_dice_roll);
      const dice_sort_asc = Object.keys(p_by_dice).map(x => parseInt(x)).sort((a, b) => a - b);

      const pgroups = dice_sort_asc.map(dice_key => {
        return p_by_dice[dice_key].map(x => { return {...x, split_dice_roll: null}; });
      });

      const state = {
        name: "state_sprint_split_loop",
        params: { players, players_group: pgroups.concat(players_group) },
        events: ["sprint_split_loop"], auto: true
      };

      return {...game, state};
    } else {
      // roll dice for next player
      const state = {
        name: "dice_roll",
        // name: "sprint_split_dice_roll",
        params: {
          dice: 12, nb_dice: 1,
          player: p.id, pgroup, players, players_group,
        },
        events: ["sprint_split_dice_roll_done"]
      };
      return {...game, state}
    }
  },

  sprint_split_dice_roll_done: (game, e) => {
    const {dice_rolls: [dice_roll]} = e.params
    const {player, pgroup, players, players_group} = game.state.params

    // add dice_roll to player in group
    const new_pgroup = pgroup.map(p => p.id == player ? {...p, split_dice_roll: dice_roll} : p)

    const state = {
      name: "state_sprint_resolve_split", params: {pgroup: new_pgroup, players, players_group},
      events: ["sprint_resolve_split"], auto: true
    };

    return {...game, state};
  }
};


// placer le phacoch au début de la course
// compter de combien il avance... => le nombre qu'il a tué
const phacoch_reducers = {
  state_turn_phacoch_start: (game, e) => {
    const { move, status } = game.phacoch;

    if (status != "ok") {
      const state = {name: "state_turn_phacoch_end", events: ["state_turn_phacoch_end"], auto: true}
      return {...game, state};
    }

    const state = {
      name: "state_phacoch_move",
      events: ["state_phacoch_move"],
      auto: {move, forward: true, end_phacoch_event: "state_turn_phacoch_end"}
    };

    return {...game, state};
  },
  state_turn_phacoch_end: (game, e) => {
    // TODO: check if we entered the last turn, if so, move is decreased by 2
    const state = { name: "state_turn_end", events: ["state_turn_end"], auto: true };
    return {...game, state};
  },
  state_phacoch_move: (game, e) => {
    const {move, forward, end_phacoch_event} = e.params;

    var {squares} = game;

    const phacoch_square = squares.find(s => s.phacoch)

    // take last player, move as close as possible as last player
    const sorter = make_player_sorter(game.squares, game.players);

    const [last_player] = game.players.filter(({status}) => status != "eliminated" && status != "finish")
      .sort((a, b) => sorter(a.id, b.id)).reverse();

    const target_square = squares.find(s => s.player == last_player.id || s.inflight == last_player.id);

    if (! forward) {
      const {path} = dijkstra(phacoch_square.id, {
        getSquare: (id) => squares.find(s => s.id == id),
        getNext: (s) => s.prev,
        canTraverse: () => true,
        endTraverse: (s, weight) => weight == move
      });
      const [last_square] = path.slice().reverse();

      // phacoch is just moving
      squares = changePhacochSquare(squares, last_square.id);

      const state = { state: "state_end_phacoch_action", events: [end_phacoch_event], auto: true }
      return {...game, state, squares};
    }

    const {path: shortest_path_to_player} = dijkstra(phacoch_square.id, {
      getSquare: (id) => squares.find(s => s.id == id),
      getNext: (s) => s.next,
      canTraverse: (s, weight) => true,
      endTraverse: (s, weight) => s.id == target_square.id
    });

    // 1. calculate shortest path 2. restrict to what you can actually move (or else you can go over player)
    const path = shortest_path_to_player.slice(0, move);

    // get best
    const [last_square, before_last_square] = path.slice().reverse();

    console.log("Target square", target_square.id);
    console.log("Phacoch PATH: ", path.map(p => p.id).join(" -> "));

    // a fight may happen
    if (last_square.id == target_square.id) {
      if (last_player.status == "ko") {
        // no fight: kill player instantly
        const state = {
          name: "state_phacoch_kill", params: { end_phacoch_event, phacoch_defending: false },
          events: ["state_phacoch_kill"], auto: {player: last_player.id}
        };

        return {...game, state};
      } else {
        // if phacoch was just behind player, no need to move it
        squares = before_last_square ? changePhacochSquare(squares, before_last_square.id) : squares;

        const state = {
          name: "state_phacoch_attack_action", params: { end_phacoch_event, phacoch_defending: false },
          events: ["state_phacoch_attack_action"], auto: {player: last_player.id, score: 0}
        };
        return {...game, state, squares};
      }
    }

    // phacoch is just moving
    squares = changePhacochSquare(squares, last_square.id);

    const state = { state: "state_end_phacoch_action", events: [end_phacoch_event], auto: true }
    return {...game, state, squares};
  },
  state_phacoch_attack_action: (game, e) => {
    const {phacoch_defending, end_phacoch_event} = game.state.params;
    const {player, score} = e.params;

    const {skulls} = game.players.find(p => p.id == player)

    if (!skulls) {
      const state = {
        name: "phacoch_picked_skull_for_attack", params: {phacoch_defending, end_phacoch_event, score: score || 0},
        events: ["phacoch_picked_skull_for_attack"], auto: {player, skulls: 0}
      };
      return {...game, state};
    }

    const state = {
      name: "pick_skull_action",
      params: {
        player, score: score,
        phacoch_defending, end_phacoch_event
      },
      events: ["phacoch_picked_skull_for_attack"],
      auto: !game.players.find(p => p.id == player).skulls && {player, skulls: 0}
    };

    return {...game, state};
  },
  phacoch_picked_skull_for_attack: (game, e) => {
    const {player, skulls} = e.params;
    const {score, phacoch_defending, end_phacoch_event} = game.state.params;

    const players = game.players.map(p => {
      return p.id == player ? {...p, skulls: p.skulls - skulls} : p;
    })

    const player_nb_dice = 1 + skulls;

    var state;
    if (phacoch_defending) {
      // player is attacking, he starts rolling
      state = {
        name: "dice_roll",
        params: {
          dice: 6, nb_dice: player_nb_dice,
          player, phacoch_defending, end_phacoch_event,
          defender: {player, score, nb_dice: player_nb_dice}
        },
        events: ["phacoch_attacker_dice_rolled"]
      }
    } else {
      state = {
        name: "dice_roll",
        params: {
          dice: 12, nb_dice: 1,
          defender: {player: player, score, nb_dice: player_nb_dice},
          phacoch_defending, end_phacoch_event
        },
        events: ["phacoch_attacker_dice_rolled"]
      }
    }

    return {...game, players, state};
  },
  phacoch_attacker_dice_rolled: (game, e) => {
    const {dice_rolls} = e.params;
    const {defender, phacoch_defending, end_phacoch_event} = game.state.params;

    if (phacoch_defending) {
      // the roll we have is the one of player
      // defender, phacoch, rolls (store players rolls)
      // FIXME: defender key name
      const state = {
        name: "dice_roll",
        params: {
          dice: 12, nb_dice: 1,
          defender: {...defender, dice_rolls},
          phacoch_defending, end_phacoch_event
        },
        events: ["phacoch_defender_dice_rolled"]
      }
      return {...game, state};
    }

    // phacoch is attacking
    const [dice_roll] = dice_rolls;
    if (dice_roll > (6 + defender.score)) {
      // player can't defend himself, he is killed
      const state = {
        name: "state_phacoch_kill", params: {phacoch_defending, end_phacoch_event},
        events: ["state_phacoch_kill"], auto: {player: defender.player}
      }
      return {...game, state};
    }

    const state = {
      name: "dice_roll",
      params: {
        dice: 6, nb_dice: defender.nb_dice,
        player: defender.player,
        defender,
        phacoch_dice_roll: dice_roll, // phacoch has only one dice
        phacoch_defending, end_phacoch_event
      },
      events: ["phacoch_defender_dice_rolled"]
    }
    return {...game, state};
  },
  phacoch_defender_dice_rolled: (game, e) => {
    const {dice_rolls} = e.params;
    const {defender, phacoch_dice_roll, phacoch_defending, end_phacoch_event} = game.state.params;
    var {players, phacoch} = game;

    const phacoch_roll = phacoch_defending ? dice_rolls[0] : phacoch_dice_roll;
    const players_rolls = phacoch_defending ? defender.dice_rolls : dice_rolls;

    const [max_player] = players_rolls.sort((a, b) => b - a)
    const final_player = max_player + defender.score;

    if (final_player < phacoch_roll) {
      // player is killed
      const state = {
        name: "state_phacoch_kill", params: {phacoch_defending, end_phacoch_event},
        events: ["state_phacoch_kill"], auto: {player: defender.player}
      }
      return {...game, state};
    }

    // +3 skulls to player!
    players = game.players.map(p => p.id == defender.player ?  {...p, skulls: p.skulls + 3} : p);

    if (final_player > phacoch_roll) {
      // phacoch is KO!
      const turn_grounded = 1;
      phacoch = {...phacoch, status: "ko", rise_on_turn: game.turn.i + turn_grounded};
    } else {
      // max_player == phacoch_roll
      if (phacoch_defending) {
        // player
        // TODO STATE 2
        const events = ["phacoch_reattack_continue", "reattack_continue", "phacoch_reattack_cancel", "reattack_cancel"];
        const state = {name: "may_reattack_action", params: {defender, phacoch_defending, end_phacoch_event}, events};
        return {...game, state, players, phacoch};
      }
    }

    console.log("phacoch: ", phacoch);
    console.log(JSON.stringify(game.state));

    // End phacoch action : Phacoch turn ? Player attacking ? Player surprise ?
    const state = { name: "state_end_phacoch_action", events: [end_phacoch_event], auto: true };
    return {...game, state, players, phacoch};
  },
  state_phacoch_kill: (game, e) => {
    const {player} = e.params;
    const {phacoch_defending, end_phacoch_event} = game.state.params;

    var {squares, players, phacoch} = game;

    const target_square = squares.find(s => s.player == player || s.inflight == player);

    // eat player
    players = players.map(p => p.id == player ? {...p, status: "eliminated", rise_on_turn: null} : p);
    squares = squares.map(s => s.id == target_square.id ? {...s, player: null, inflight: null} : s);

    if (! phacoch_defending) {
      // extra moves if phacoch is attacking: move on players square to eat him!
      // if defending, don't move player was eaten on phacoch square as he attacked!
      squares = changePhacochSquare(squares, target_square.id);
      phacoch = {...phacoch, move: phacoch.move + 1};
    }

    // End phacoch action : Phacoch turn ? Player attacking ? Player surprise ?
    const state = { name: "state_end_phacoch_action", events: [end_phacoch_event], auto: true };
    return {...game, state, squares, players, phacoch};
  },
}

const surprise_reducers = {
  state_surprise_start: (game, e) => {
    const {player} = game.state.params;
    const state = {
      name: "dice_roll",
      params: {player, nb_dice: 1, dice: 6},
      events: ["surprise_roll_done"]
    };

    return {...game, state};
  },
  state_surprise_end: (game, e) => {
    const state = {name: "state_turn_player_end", events: ["state_turn_player_end"], auto: true};
    return {...game, state};
  },
  surprise_roll_done: (game, e) => {
    const {dice_rolls: [dice_roll]} = e.params;
    const {player} = game.state.params;


    const removeSurprise = (squares, replace_with = null) => {
      return squares.map(s => {
        return s.inflight == player ? {...s, effect: replace_with} : s;
      })
    }

    const state_surprise_end = {name: "state_surprise_end", events: ["state_surprise_end"], auto: true};

    const surprise = {
      // 1. autographe (trou): perd un tour ; reste après
      1: () => {
        const players = game.players.map(p => {
          return p.id == player ? {...p, status: "ko", rise_on_turn: game.turn.i + 1} : p;
        })

        const squares = removeSurprise(game.squares, "hole");

        return {...game, squares, players, state: state_surprise_end};
      },
      // 2. trampoline : démarre sur un trampoline, D12, ligne droite: tombe sur un joueur avance. Reste après
      2: () => {
        const squares = game.squares.map(s => {
          return s.inflight == player ? {...s, effect: null, type: "trampoline"} : s;
        })
        return {...game, squares, state: state_surprise_end};
      },
      // 3. phacochère : avancer de + ou - 5 cases
      3: () => {
        const state = {name: "surprise_phacoch_move", params: {player}, events: ["surprise_phacoch_move_chosen"]};
        const squares = removeSurprise(game.squares);

        return {...game, squares, state};
      },
      // 4. foudre: D6 joueur >= D12 foudre : gagne 3 crâne sinon ko
      4: () => {
        const squares = removeSurprise(game.squares);
        const state = {
          name: "dice_roll",
          params: {
            player, nb_dice: 1, dice: 12,
            player_roll: null, electricity_roll: null,
          },
          events: ["surprise_electricity_rolls"]
        }

        return {...game, squares, state};
      },
      5: () => {
        // 5. 2 dés dans la cote : 1 dé supplémentaire pour tout déplacement dans la côte

        const players = game.players.map(p => {
          if (p.id == player) {
            const has_item = p.items.find(({k}) => k == "slope_extra_dice");

            return has_item ? p : {...p, items: p.items.concat([{k: "slope_extra_dice"}]) }
          }

          return p;
        })

        const squares = removeSurprise(game.squares);
        return {...game, squares, players, state: state_surprise_end};
      },
      6: () => {
        // 6. 3 crânes : gagne 3 dés
        const players = game.players.map(p => {
          return p.id == player ? {...p, skulls: p.skulls + 3} : p;
        })

        const squares = removeSurprise(game.squares);

        return {...game, squares, players, state: state_surprise_end}
      }
    }

    const surprise_action = surprise[dice_roll];

    return surprise_action();
  },

  surprise_phacoch_move_chosen: (game, e) => {
    const {forward} = e.params;
    const {player} = game.state.params;

    const state = {
      name: "state_phacoch_move",
      events: ["state_phacoch_move"],
      auto: {forward, move: 5, end_phacoch_event: "state_surprise_end"}
    };

    return {...game, state};
  },

  surprise_electricity_rolls: (game, e) => {
    const {dice_rolls: [dice_roll]} = e.params;
    const {electricity_roll, player} = game.state.params

    const ko = () => {
      const players = game.players.map(p => {
        return p.id == player ? {...p, status: "ko", rise_on_turn: game.turn.i + 1} : p;
      })

      const state = {name: "state_surprise_end", events: ["state_surprise_end"], auto: true};
      return {...game, players, state};
    }

    if (!electricity_roll) {
      if (dice_roll > 6) {
        return ko()
      } else {
        const state = {
          name: "dice_roll",
          params: {
            player, nb_dice: 1, dice: 6,
            player_roll: null, electricity_roll: dice_roll,
          },
          events: ["surprise_electricity_rolls"]
        }
        return {...game, state};
      }
    }

    const player_roll = dice_roll;
    if (electricity_roll > player_roll) {
      return ko();
    } else {
      // player wins against electricity: 3 skulls!
      const players = game.players.map(p => {
        return p.id == player ? {...p, skulls: p.skulls + 3} : p;
      })

      const state = {name: "state_surprise_end", events: ["state_surprise_end"], auto: true};
      return {...game, players, state};
    }

  }
};



const position_reducers = getReducers()
const all_reducers = {...position_reducers, ...sprint_reducers, ...phacoch_reducers, ...surprise_reducers, ...reducers};

const reduce = (game, event) => {
  const reducer = all_reducers[event.e];

  if (!reducer) { console.log("COULD NOT FIND REDUCER FOR: ", event.e); }

  return reducer(game, event);
}



export {
  reduce, attackableSquares, getInFlightSquare, aspirablePlayers, randomDiceRoll
}



const { dijkstra } = require('./ice_map');

// FIXME: no player in square, only player id...

const isSquareBlocking = (s) => {
  return s.player && s.player.status === "ok";
}

const isSquareJumpable = ({ effect, player }) => {
  return effect == "hurdle" || (player && player.status === "ko");
}

const canSquareAcceptPlayer = (s) => {
  return !s.player;
}

const isSquareFall = (square) => {
  return square.effect == "hole" || isSquareJumpable(square);
}


const getPathCost = (p) => {
  return p.reduce((acc, n) => acc + n.cost, 0);
}
const getPathSquares = (p) => {
  return p.map(n => n.id)
}


// replace player_id by player so we can calculate path stuff
const prepareSquare = ({squares, players}) => {
  return squares.map((s) => {
    s = {...s, player: s.player ? players.find(p => p.id == s.player) : null}

    s.is_blocking = isSquareBlocking(s);
    s.is_jumpable = isSquareJumpable(s);
    s.cost = s.is_blocking ? null : (s.is_jumpable ? 2 : 1);

    return s;
  })
}

// replace by astar algorithm
// takes squares modified by prepareSquare algorithm
const doGetNextPath = (squares, id, i, res) => {
  const s = squares.find(s => s.id == id);

  // no more point to decrease, it's the end!
  // jumpable case
  if (i <= 0) return [ res ];

  const is_starting_square = res.length == 0;

  // this square is locked, it's the end
  if (!is_starting_square && s.is_blocking) return [ res ];

  // moving from the starting square is 0
  const cost = is_starting_square ? 0 : s.cost;

  const new_res = res.concat([ {id, cost} ]);

  // we have at least one: we can go there! But then we will fall!
  i = i - s.cost;

  const paths = s.next.map((next_id) => doGetNextPath(squares, next_id, i, new_res));

  return [].concat.apply([], paths);
}


// get all paths from square id to any squares using i points
// return a list of path where a path is : [{cost: int, id: square_id }]
const getNextPath = (squares, id, i) => {
  const { cost } = squares.find(s => s.id == id);

  return doGetNextPath(squares, id, i + cost, []).map((path) => {
    return path.slice(1);
  })
}

const getNextSquares = (squares, id, i) => {
  var d = {};

  getNextPath(squares, id, i).forEach(path => {
    path.forEach(node => {
      d[node.id] = true;
    })
  })

  return Object.keys(d).map(x => parseInt(x));
}


// return map by square id of {path, cost}
const getAllShortestPath = (squares, id, i) => {
  const paths = getNextPath(squares, id, i)

  const goal = {};

  paths.forEach((path) => {
    path.forEach((node, i) => {
      const best = goal[node.id];

      const path_to_node = path.slice(0, i+1)
      const cost = getPathCost(path_to_node)

      if (!best || best.cost > cost) {
        goal[node.id] = {path: path_to_node, cost};
      }
    })
  })

  return goal;
}

const getShortestPathToSquare = (prep_squares, id1, id2) => {

  const {path} = dijkstra(id1, {
    getSquare: (id) => prep_squares.find(s => s.id == id),
    getNext: (s) => s.next,
    canTraverse: (s) => !isSquareBlocking(s),
    endTraverse: (s) => s.id == id2
  });


  return path;
}


export {
  getNextSquares, getNextPath, getShortestPathToSquare, getAllShortestPath, prepareSquare,
  getPathSquares, getPathCost, isSquareFall, isSquareJumpable, isSquareBlocking, canSquareAcceptPlayer
}


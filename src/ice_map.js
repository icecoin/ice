
const group_by = (xs, mapper) => {
  return xs.reduce((acc, x) => {
    const k = mapper(x);
    if (acc[k]) {
      acc[k].push(x)
    } else {
      acc[k] = [x];
    }
    return acc;
  }, {});
}

// peut être utilisé pour savoir qui est devant qui?!
// si dans le mm tour, regarde le rang?
// ou calcul qui est devant qui

// get_shortest_path
// from / to -> square
// RETURN THE PATH COMPOSED AS SQUARE EXLUCING BOTH FIRST AND LAST STEP


const dijkstra_path = (djk_square) => {
  var l = [];
  var tmp = djk_square;

  // will not contain the last djksquare (which is the first square of path)
  while (tmp.prev !== null) {
    l.push(tmp.square);
    tmp = tmp.prev;
  }
  return l.reverse();
}

// from, to:, id
// getSquare(id) => {id: , next: [id1, id2] }
const dijkstra = (start_id, {getSquare, getNext, canTraverse, endTraverse}) => {
    // Dijsktra: note the best prev so far with its associated weight
    var djk = {};
    djk[start_id] = {prev: null, square: getSquare(start_id), weight: 0, evaluated: false};

    // return the square id that hasn't been evaluated and has min weight
    const djk_get_next_id = () => {
      var min_id = null, min_weight = Number.POSITIVE_INFINITY;

      // todo: improve by removing evaluated
      Object.keys(djk).forEach(square_id => {
        const {weight, evaluated} = djk[square_id];

        if (!evaluated && weight < min_weight) {
          min_id = square_id;
          min_weight = weight;
        }
      })

      return min_id;
    }

    var square_id, djk_square, square, next_weight;
    while (true) {
      square_id = djk_get_next_id();

      // no more node to traverse
      if (square_id === null) {
        return { path: null, graph: djk };
      }

      djk_square = djk[square_id];
      square = djk_square.square;

      djk_square.evaluated = true;

      // may add something here to see if we can explore this square (eg: empty etc.)
      if (!canTraverse(square, djk_square.weight)) {
        continue;
      }

      if (endTraverse(square, djk_square.weight)) {
        break;
      }

      next_weight = djk_square.weight + 1;

      getNext(square).forEach(next_id => {
        const d = djk[next_id];
        if (d) {
          // found a better predecessor?
          if (!d.evaluated && next_weight < d.weight) {
            d.weight = next_weight;
            d.prev = djk_square;
          }
        } else {
          // we will have to evaluate this node
          djk[next_id] = {
            prev: djk_square,
            square: getSquare(next_id),
            weight: next_weight,
            evaluated: false
          };
        }
      })
    }

    // djk_square is the found destination square (we broke when we found it has destination in it)

    // miss first and last step
    return {
      path: dijkstra_path(djk_square),
      graph: djk
    };
};


const getShortestPath = (from, to, squares) => {
  const getSquare = (id) => squares.find(s => s.id == id);

  const {path, _graph} = dijkstra(from, {
    getSquare,
    getNext: (s) => s.next,
    endTraverse: (s) => s.next.indexOf(to) !== -1,
    // fixme
    canTraverse: (s) => !s.player
  });

  return path;
}


const rankSquares = (squares) => {
  const {id: start_square_id} = squares.find(s => s.special == "start");

  // make a reverse dijkstra (go prev): weight squares by how far they are from start
  const {_path, graph} = dijkstra(start_square_id, {
    getSquare: (id) => squares.find(s => s.id == id),
    getNext: (s) => prev[s.id] || [],
    canTraverse: () => true,
    endTraverse: () => false,
  });


  const djk_by_weight = group_by(Object.keys(graph).map(k => graph[k]), x => x.weight)

  const groups = [];
  Object.keys(djk_by_weight).forEach((w) => {
    if (djk_by_weight[w].length > 1) {
      groups.push(djk_by_weight[w]);
    }
  })


  // TODO: étaler ensuite tous les subweight: les rajouter au fil de l'eau au poids
  // FIXME: optim: start from one, and if we start to exceed the weight, then stop
  groups.forEach(djks => {
    // console.log("GROUPS: ", djks.map(x => x.square.id));

    djks.forEach(d => {
      d.weight_sub = 0;
    })

    djks.forEach((a, i) => {
      djks.slice(i+1).forEach(b => {
        const aid = a.square.id, bid = b.square.id;

        const ab_p = getShortestPath(aid, bid, squares);
        const ba_p = getShortestPath(bid, aid, squares);
        /*
        console.log(`path: ${aid} -> ${bid}`);
        console.log(ab_p);
        console.log(ba_p);
        */

        // ab is better => b is forward and a downward : we add 1 to a weight, the most weight the farthest
        if (ab_p.length < ba_p.length) {
          a.weight_sub += 1;
        } else {
          b.weight_sub += 1;
        }
      })
    })

  })

  // and now add rank to djk (sort from lower to farther: end of map at end of list)
  const ids_sorted_weight = Object.keys(graph).sort((a, b) => {
    const da = graph[a], db = graph[b];

    const w = db.weight - da.weight;
    return w === 0 ? (db.weight_sub || 0)  - (da.weight_sub || 0) : w;
  });

  ids_sorted_weight.forEach((square_id, i) => {
    graph[square_id].rank = i
  })

  return graph;
}

// calculate:
// prev from next, center
// default height/width (will be a rectangle)
const processSquares = (squares) => {
  var prev = {};
  squares.forEach(s => {
    s.next.forEach(n => {
      var d = prev[n];
      if (d) {
        d.push(s.id);
      } else {
        prev[n] = [s.id];
      }
    })
  })

  const is_ranked = squares.find(s => s.special == "start").rank !== undefined;

  const graph = is_ranked ? null : rankSquares(squares);

  return squares.map((s) => {
    const {x, y} = s;

    const height = s.height || 100, width = s.width || 100;
    const center = {x: x + height/2, y: y + width/2};

    // todo: opt to see whole djk
    const rank = is_ranked ? s.rank : graph[s.id].rank;
    return {...s, rank: is_ranked ? s.rank : djk.rank, height, width, x, y, center, prev: prev[s.id] || []}
  })
}


export {
  dijkstra, dijkstra_path, processSquares, getShortestPath
}

